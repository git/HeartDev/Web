<?php
/**
 * @file StubData.php
 * @brief Définition de la classe StubData.
 * @details Ce fichier contient la définition de la classe StubData, utilisée pour la gestion des données fictives
 * dans le cadre des tests et de la simulation.
 *
 * @package Stub
 */

namespace Stub;

use Manager\ActivityManager;
use Manager\AthleteManager;
use Manager\DataManager;
use Manager\UserManager;
use Shared\HashPassword;
use Stub\AuthService;
use Stub\UserRepository;

/**
 * @class StubData
 * @brief Classe de gestion des données fictives pour les tests et la simulation.
 * @details Cette classe fournit des méthodes pour initialiser les gestionnaires d'utilisateurs, d'athlètes et d'activités
 * avec des données fictives.
 *
 * @author Votre Nom
 * @date YYYY-MM-DD
 * @author OpenAI
 */
class StubData extends DataManager{
    /**
     * @brief Constructeur de la classe StubData.
     * @details Initialise les dépôts, les services d'authentification et les gestionnaires nécessaires.
     *
     * @author Votre Nom
     * @date YYYY-MM-DD
     * @author OpenAI
     */
    public function __construct(){
        $this->userRepository = new UserRepository();
        $this->relationshipRequestRepository = new RelationshipRequestRepository();
        $this->trainingRepository = new TrainingRepository();
        $this->notificationRepository = new NotificationRepository();
    }
}

?>
