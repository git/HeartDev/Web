<?php

namespace Stub;

use Model\Notification;
use Model\Observable;
use Model\Observer;
use Model\RelationshipRequest;
use Network\INotificationService;
use Repository\INotificationRepository;
use Repository\IUserRepository;

class NotificationService implements INotificationService
{

    private INotificationRepository $repository;
    private IUserRepository $userRepository;

    public function __construct(INotificationRepository $repository,IUserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;

    }

    public function sendNotification($toUserId, $notification)
    {
        $this->repository->addItem($notification);
        $this->userRepository->getItemById($toUserId)->addNotification($notification);
    }

    public function update(Observable $observable)
    {
        echo "======================";
        echo "======================";
        var_dump("999999999999999");
        if ($observable instanceof RelationshipRequest) {
            $request = $observable;
            switch ($request->getStatus()) {
                case 'pending':
                    $msg = "You have a new friend request from user " . $request->getFromUser();
                    $notification = new Notification($request->getToUser(),"social", $msg);
                    $this->sendNotification($request->getToUser(), $notification);
                    break;
                case 'accepted':
                    $msg = "User " . $request->getToUser() . " has accepted your request.";
                    $notification = new Notification($request->getFromUser(),"social", $msg);
                    $this->sendNotification($request->getFromUser(), $notification);
                    break;
                case 'declined':
                    $msg = "User " . $request->getToUser() . " has declined your request.";
                    $notification = new Notification($request->getFromUser(),"social", $msg);
                    $this->sendNotification($request->getFromUser(), $notification);
                    break;
            }
        }
    }


}

