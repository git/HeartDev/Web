<?php

namespace Stub;

use Repository\INotificationRepository;
use Model\Notification;

class NotificationRepository implements INotificationRepository
{
    private $notifications = []; // Array to store notifications
    public function __construct()
    {
        // Initialize with some sample notifications for user IDs 1, 2, and 3
        $this->notifications[] = new Notification(1, 'info', 'Welcome to our service!');
        $this->notifications[] = new Notification(2, 'alert', 'Your subscription is about to expire.');
        $this->notifications[] = new Notification(3, 'info', 'New features available.');
        $this->notifications[] = new Notification(1, 'reminder', 'Don’t forget your upcoming appointment.');
        $this->notifications[] = new Notification(2, 'update', 'Service update completed.');
        // Add more notifications as needed
    }
    public function getItemById(int $id)
    {
        // Assuming each notification has a unique ID
        foreach ($this->notifications as $notification) {
            if ($notification->getId() == $id) {
                return $notification;
            }
        }
        return null;
    }

    public function getNbItems(): int
    {
        return count($this->notifications);
    }

    public function getItems(int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array
    {
        // For simplicity, returning a slice of the array
        return array_slice($this->notifications, $index, $count);
    }

    public function getItemsByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array
    {
        $filtered = array_filter($this->notifications, function($notification) use ($substring) {
            return strpos($notification->getMessage(), $substring) !== false;
        });

        // For simplicity, just returning a slice without ordering
        return array_slice($filtered, $index, $count);
    }

    public function getItemByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false)
    {
        $items = $this->getItemsByName($substring, $index, $count, $orderingPropertyName, $descending);
        return $items ? $items[0] : null;
    }

    public function updateItem($oldItem, $newItem): void
    {
        // Find and update the item. This is a simplistic implementation.
        foreach ($this->notifications as $key => $notification) {
            if ($notification === $oldItem) {
                $this->notifications[$key] = $newItem;
                break;
            }
        }
    }

    public function addItem($item): void
    {
        $this->notifications[] = $item;
    }

    public function deleteItem($item): bool
    {
        foreach ($this->notifications as $key => $notification) {
            if ($notification === $item) {
                unset($this->notifications[$key]);
                return true;
            }
        }
        return false;
    }
}
