<?php
/**
 * Nom du fichier: UserRepository.php
 *
 * @author PEREDERII Antoine
 * @date 2023-11-25
 * @description Classe représentant un dépôt d'utilisateurs pour l'accès aux données des utilisateurs.
 *
 * @package repository
 */

namespace Stub;

use Model\Athlete;
use Model\CoachAthlete;
use Model\User;
use Repository\IUserRepository;

/**
 * @class UserRepository
 * @brief Classe représentant un dépôt d'utilisateurs pour l'accès aux données des utilisateurs.
 */
class UserRepository implements IUserRepository {
    private array $users = [];

    /**
     * Constructeur de la classe UserRepository.
     */
    public function __construct() {
        $this->users[] = new User(1, "Doe", "John", "Doe","john.doe@example.com", "password123", 'M', 1.80, 75, new \DateTime("1985-05-15"), new CoachAthlete());
        $this->users[] = new User(2, "Smith", "Jane","Smith", "jane.smith@example.com", "secure456", 'F', 1.65, 60, new \DateTime("1990-03-10"), new Athlete());
        $this->users[] = new User(3, "Martin", "Paul","Martin", "paul.martin@example.com", "super789", 'M', 1.75, 68, new \DateTime("1988-08-20"), new CoachAthlete());
        $this->users[] = new User(4, "Brown", "Anna","Brown", "anna.brown@example.com", "test000", 'F', 1.70, 58, new \DateTime("1992-11-25"), new Athlete());
        $this->users[] = new User(5, "Lee", "Bruce","Lee", "bruce.lee@example.com", "hello321", 'M', 1.72, 70, new \DateTime("1970-02-05"), new Athlete());
    }

    /**
     * Obtient un utilisateur par son identifiant.
     *
     * @param int $id L'identifiant de l'utilisateur.
     * @return User|null L'utilisateur correspondant à l'identifiant donné, ou null s'il n'existe pas.
     */
    public function getItemById(int $id): ?User {
        foreach ($this->users as $user) {
            if ($user->getId() === $id) {
                return $user;
            }
        }
        return null;
    }

    /**
     * Obtient un utilisateur par son rôle.
     *
     * @return User|null L'utilisateur ayant le rôle spécifié, ou null s'il n'y en a pas.
     */
    public function getItemByRole(): ?User {
        foreach ($this->users as $user) {
            if ($user->getRole() instanceof \Model\Coach) {
                return $user;
            }
        }
        return null;
    }

    /**
     * Obtient un utilisateur par son adresse e-mail.
     *
     * @param string $email L'adresse e-mail de l'utilisateur.
     * @return User|null L'utilisateur correspondant à l'adresse e-mail donnée, ou null s'il n'existe pas.
     */
    public function getItemByEmail(string $email): ?User {
        foreach ($this->users as $user) {
            if ($user->getEmail() === $email) {
                return $user;
            }
        }
        return null;
    }


    /**
     * Obtient le nombre total d'utilisateurs dans le dépôt.
     *
     * @return int Le nombre total d'utilisateurs.
     */
    public function getNbItems(): int {
        return count($this->users);
    }

    public function getItems(int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array {
        // Cette méthode est un exemple simple, on ne gère pas l'ordonnancement ici
        return array_slice($this->users, $index, $count);
    }

    /**
     * Obtient un tableau d'utilisateurs dont le nom ou le prénom contient la sous-chaîne spécifiée.
     *
     * @param string $substring La sous-chaîne pour la recherche.
     * @param int $index L'indice de départ dans le tableau.
     * @param int $count Le nombre d'utilisateurs à récupérer.
     * @param string|null $orderingPropertyName Le nom de la propriété pour l'ordonnancement (facultatif).
     * @param bool $descending Indique si l'ordonnancement doit être effectué en ordre descendant (facultatif, par défaut false).
     * @return array|null Le tableau d'utilisateurs filtré, ou null s'il n'y a aucun utilisateur correspondant.
     */
    public function getItemsByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array {
        $filteredUsers = array_filter($this->users, function ($user) use ($substring) {
            return str_contains(strtolower($user->getNom()), strtolower($substring)) || str_contains(strtolower($user->getPrenom()), strtolower($substring));
        });
        return array_slice($filteredUsers, $index, $count);
    }

    /**
     * Obtient un utilisateur dont le nom ou le prénom contient la sous-chaîne spécifiée.
     *
     * @param string $substring La sous-chaîne pour la recherche.
     * @param int $index L'indice de départ dans le tableau.
     * @param int $count Le nombre d'utilisateurs à récupérer.
     * @param string|null $orderingPropertyName Le nom de la propriété pour l'ordonnancement (facultatif).
     * @param bool $descending Indique si l'ordonnancement doit être effectué en ordre descendant (facultatif, par défaut false).
     * @return User|null L'utilisateur filtré, ou null s'il n'y a aucun utilisateur correspondant.
     */
    public function getItemByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): ?User {
        $filteredUsers = $this->GetItemsByName($substring, $index, $count, $orderingPropertyName, $descending);
        return $filteredUsers[0] ?? null;
    }
    /**
     * Met à jour un utilisateur existant dans le dépôt.
     *
     * @param User $oldItem L'ancienne instance de l'utilisateur.
     * @param User $newItem La nouvelle instance de l'utilisateur.
     * @return void
     */
    public function updateItem($oldUser, $newUser): void {
        $index = array_search($oldUser, $this->users);
        if ($index !== false) {
            $this->users[$index] = $newItem;
        }
    }

    /**
     * Ajoute un nouvel utilisateur au dépôt.
     *
     * @param User $user L'instance de l'utilisateur à ajouter.
     * @return void
     */
    public function addItem( $user): void {
        $this->users[] = $user;
    }

    /**
     * Supprime un utilisateur du dépôt.
     *
     * @param User $user L'instance de l'utilisateur à supprimer.
     * @return bool Retourne true si la suppression a réussi, sinon false.
     */
    public function deleteItem( $user): bool {
        $index = array_search($user, $this->users);
        if ($index !== false) {
            unset($this->users[$index]);
            return true;
        }
        return false;
    }

    public function addFriend(int $user1, int $user2)
    {
        return true;
    }


    public function deleteFriend(int $user1, int $user2)
    {
       return true;
    }
}
?>
