<?php

namespace Stub;

use Model\RelationshipRequest;
use Repository\IRelationshipRequestRepository;

class RelationshipRequestRepository implements IRelationshipRequestRepository {
    private array $requests = [];

    public function __construct() {
        // Example Relationship Requests
       /* $this->requests[] = new RelationshipRequest(1, 1, 2); // Request from User 1 to User 2
        $this->requests[] = new RelationshipRequest(2, 3, 4); // Request from User 3 to User 4*/
        // ... more sample requests as needed
    }

    public function getItemById(int $id): ?RelationshipRequest {
        foreach ($this->requests as $request) {
            if ($request->id === $id) {
                return $request;
            }
        }
        return null;
    }

    public function getNbItems(): int {
        return count($this->requests);
    }

    public function getItems(int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array {
        // Cette méthode est un exemple simple, on ne gère pas l'ordonnancement ici
        return array_slice($this->requests, $index, $count);
    }

    public function getItemsByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array {
        throw new \Exception('Search Not applicable on this entity');
    }

    public function getItemByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false) {
        throw new \Exception('Search Not applicable on this entity');
    }

    public function updateItem($oldRequest, $newRequest): void {
        $index = array_search($oldRequest, $this->requests);
        if ($index !== false) {
            $this->requests[$index] = $newRequest;
        }
    }

    public function addItem($request): void {
        $this->requests[] = $request;
    }

    public function deleteItem($request): bool {
        $index = array_search($request, $this->requests);
        if ($index !== false) {
            unset($this->requests[$index]);
            return true;
        }
        return false;
    }
}