<?php

namespace Model;

abstract class Observable {
    private array $observers = array();

    public function attachObserver(Observer $observer) {
        $this->observers[] = $observer;
    }

    public function detachObserver(Observer $observer) {
        $key = array_search($observer, $this->observers);
        if ($key !== false) {
            unset($this->observers[$key]);
        }
    }

    public function notifyObservers() {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}
?>