<?php

namespace Model;

class Notification
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    private string $type;
    private string $message;
    private int $toUserId;

    /**
     * @return int
     */
    public function getToUserId(): int
    {
        return $this->toUserId;
    }

    /**
     * @param int $toUserId
     */
    public function setToUserId(int $toUserId): void
    {
        $this->toUserId = $toUserId;
    }
    /**
     * @param string $type
     * @param string $message
     */
    public function __construct(int $toUserId,string $type, string $message)
    {
        $this->type = $type;
        $this->toUserId =$toUserId;
        $this->message = $message;
    }
    public function __toString(): string
    {
        return var_export($this, true);
    }
}