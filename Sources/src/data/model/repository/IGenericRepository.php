<?php
namespace Repository;

interface IGenericRepository
{
    public function getItemById(int $id);
    public function getNbItems(): int;
    public function getItems(int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array;
    public function getItemsByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false): array;
    public function getItemByName(string $substring, int $index, int $count, ?string $orderingPropertyName = null, bool $descending = false);
    public function updateItem($oldItem, $newItem) : void;
    public function addItem($item) : void;
    public function deleteItem($item): bool;
}

?>