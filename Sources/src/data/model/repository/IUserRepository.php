<?php
namespace Repository;

interface IUserRepository extends IGenericRepository {
   public function addFriend(int $user1,int $user2);
    public function deleteFriend(int $user1,int $user2);

}

?>