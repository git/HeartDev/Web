<?php
/**
 * Nom du fichier: Role.php
 *
 * @author PEREDERII Antoine
 * @date 2023-11-25
 * @description Classe abstraite Role représentant le rôle d'un utilisateur.
 *
 * @package model
 */

namespace Model;

use Stub\TrainingRepository;

/**
 * @class Role
 * @brief Classe abstraite représentant le rôle d'un utilisateur.
 */
abstract class Role {
    protected int $id;
    protected array $usersList = [];
    protected array $usersRequests = [];
    protected array $trainingList = [];

    // Attributs spécifiques au Coach si nécessaire
    /**
     * Obtient la liste des utilisateurs associés au rôle.
     *
     * @return array|null La liste des utilisateurs associés au rôle, ou null si aucune.
     */
    public function getUsersList(): ?array
    {
        return $this->usersList;
    }
    public function getUsersRequests(): ?array
    {
        return $this->usersRequests;
    }
    public function addUsersRequests(RelationshipRequest $request): void
    {
        $this->usersRequests [] = $request;
    }
    public function removeRequest(RelationshipRequest $req): bool
    {

        $key = array_search($req, $this->usersRequests);
        if ($key !== false) {
            array_splice($this->usersRequests, $key, 1);
            return true;
        }

        return false;
    }
    /**
     * Vérifie si un utilisateur peut être ajouté au rôle.
     *
     * @param User $user L'utilisateur à vérifier.
     * @return bool True si l'utilisateur peut être ajouté, sinon false.
     */
    public abstract function CheckAdd(User $user) : bool;
    /**
     * Ajoute un utilisateur à la liste des utilisateurs associés au rôle.
     *
     * @param User $user L'utilisateur à ajouter.
     * @return bool True si l'ajout est réussi, sinon false.
     */
    public function addUser(User $user): bool
    {
        if($this->CheckAdd($user)){
            array_push($this->usersList, $user);
            return true;
        }
        return false;
    }
    /**
     * Supprime un utilisateur de la liste des utilisateurs associés au rôle.
     *
     * @param User $user L'utilisateur à supprimer.
     * @return bool True si la suppression est réussie, sinon false.
     */
    public function removeUser(User $user): bool
    {
        if($this->CheckAdd($user)){
            $key = array_search($user, $this->usersList);
            if ($key !== false) {
                array_splice($this->usersList, $key, 1);
                return true;
            }
        }
        return false;
    }
    /**
     * Ajoute un entraînement à la liste des entraînements associés au rôle.
     *
     * @param Training $training L'entraînement à ajouter.
     * @return bool True si l'ajout est réussi, sinon false.
     */
    public function addTraining(Training $training)
    {
        $this->trainingList [] = $training;
        return true;
    }
    public function getTrainingsList(): array
    {
        return $this->trainingList;
    }
}
?>