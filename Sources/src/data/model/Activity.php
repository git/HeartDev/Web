<?php
/**
 * Nom du fichier: Activity.php
 *
 * @author PEREDERII Antoine
 * @date 2023-11-25
 * @description Classe Activity représentant une activité physique.
 *
 * @package model
 */

namespace Model;

/**
 * @class Activity
 * @brief Classe représentant une activité physique.
 */
class Activity
{
    private static int $lastId = 0;
    private int $idActivity;
    private String $type;
    private \DateTime $date;
    private \DateTime $heureDebut;
    private \DateTime $heureFin;
    private int $effortRessenti;
    private float $variability;
    private float $variance;
    private float $standardDeviation;
    private int $average;
    private int $maximum;
    private int $minimum;
    private float $avrTemperature;
    private bool $hasAutoPause;

    /**
     * Constructeur de la classe Activity.
     *
     * @param String $type Le type d'activité.
     * @param \DateTime $date La date de l'activité.
     * @param \DateTime $heureDebut L'heure de début de l'activité.
     * @param \DateTime $heureFin L'heure de fin de l'activité.
     * @param int $effortRessenti L'effort ressenti pour l'activité (entre 0 et 5).
     * @param float $variability La variabilité de la fréquence cardiaque.
     * @param float $variance La variance de la fréquence cardiaque.
     * @param float $standardDeviation L'écart type de la fréquence cardiaque.
     * @param int $average La moyenne de la fréquence cardiaque.
     * @param int $maximum La fréquence cardiaque maximale.
     * @param int $minimum La fréquence cardiaque minimale.
     * @param float $avrTemperature La température moyenne pendant l'activité.
     * @param bool $hasAutoPause Indique si la pause automatique est activée.
     *
     * @throws \Exception Si l'effort ressenti n'est pas compris entre 0 et 5.
     */
    public function __construct(
        String $type,
        \DateTime $date,
        \DateTime $heureDebut,
        \DateTime $heureFin,
        int $effortRessenti,
        float $variability,
        float $variance,
        float $standardDeviation,
        float $average,
        int $maximum,
        int $minimum,
        float $avrTemperature,
        bool $hasPause
    ) {
        $this->idActivity = self::generateId();
        $this->type = $type;
        $this->date = $date;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;

        if ($effortRessenti < 0 || $effortRessenti > 5) {
            throw new \Exception("L'effort ressenti doit être compris entre 0 et 5");
        }

        $this->effortRessenti = $effortRessenti;
        $this->variability = $variability;
        $this->variance = $variance;
        $this->standardDeviation = $standardDeviation;
        $this->average = $average;
        $this->maximum = $maximum;
        $this->minimum = $minimum;
        $this->avrTemperature = $avrTemperature;
        $this->hasAutoPause = $hasPause;
    }

    /**
     * Génère un identifiant unique pour l'activité.
     *
     * @return int L'identifiant unique généré.
     */
    private static function generateId(): int
    {
        self::$lastId++;
        return self::$lastId;
    }

    /**
     * Obtient l'identifiant de l'activité.
     *
     * @return int L'identifiant de l'activité.
     */
    public function getIdActivity(): int
    {
        return $this->idActivity;
    }

    /**
     * Obtient le type d'activité.
     *
     * @return String Le type d'activité.
     */
    public function getType(): String
    {
        return $this->type;
    }

    /**
     * Obtient la date de l'activité.
     *
     * @return \DateTime La date de l'activité.
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * Obtient l'heure de début de l'activité.
     *
     * @return \DateTime L'heure de début de l'activité.
     */
    public function getHeureDebut(): \DateTime
    {
        return $this->heureDebut;
    }

    /**
     * Obtient l'heure de fin de l'activité.
     *
     * @return \DateTime L'heure de fin de l'activité.
     */
    public function getHeureFin(): \DateTime
    {
        return $this->heureFin;
    }

    /**
     * Obtient l'effort ressenti pour l'activité.
     *
     * @return int L'effort ressenti pour l'activité.
     */
    public function getEffortRessenti(): int
    {
        return $this->effortRessenti;
    }

    /**
     * Obtient la variabilité de la fréquence cardiaque.
     *
     * @return float La variabilité de la fréquence cardiaque.
     */
    public function getVariability(): float
    {
        return $this->variability;
    }

    /**
     * Obtient la variance de la fréquence cardiaque.
     *
     * @return float La variance de la fréquence cardiaque.
     */
    public function getVariance(): float
    {
        return $this->variance;
    }

    /**
     * Obtient l'écart type de la fréquence cardiaque.
     *
     * @return float L'écart type de la fréquence cardiaque.
     */
    public function getStandardDeviation(): float
    {
        return $this->standardDeviation;
    }

    /**
     * Obtient la moyenne de la fréquence cardiaque.
     *
     * @return float La moyenne de la fréquence cardiaque.
     */
    public function getAverage(): float
    {
        return $this->average;
    }

    /**
     * Obtient la fréquence cardiaque maximale.
     *
     * @return int La fréquence cardiaque maximale.
     */
    public function getMaximum(): int
    {
        return $this->maximum;
    }

    /**
     * Obtient la fréquence cardiaque minimale.
     *
     * @return int La fréquence cardiaque minimale.
     */
    public function getMinimum(): int
    {
        return $this->minimum;
    }

    /**
     * Obtient la température moyenne pendant l'activité.
     *
     * @return float La température moyenne pendant l'activité.
     */
    public function getAvrTemperature(): float
    {
        return $this->avrTemperature;
    }

    /**
     * Modifie le type d'activité.
     *
     * @param String $type Le nouveau type d'activité.
     */
    public function setType(String $type): void
    {
        $this->type = $type;
    }

    /**
     * Modifie l'effort ressenti pour l'activité.
     *
     * @param int $effortRessenti Le nouvel effort ressenti.
     * @throws \Exception Si l'effort ressenti n'est pas compris entre 0 et 5.
     */
    public function setEffortRessenti(int $effortRessenti): void
    {
        if ($effortRessenti < 0 || $effortRessenti > 5) {
            throw new \Exception("L'effort ressenti doit être compris entre 0 et 5");
        }

        $this->effortRessenti = $effortRessenti;
    }

    /**
     * Convertit l'objet Activity en chaîne de caractères.
     *
     * @return String La représentation de l'activité sous forme de chaîne de caractères.
     */
    public function __toString(): String
    {
        return "Activité n°" . $this->idActivity . " : " .
            $this->type . " le " . $this->date->format('d/m/Y') .
            " de " . $this->heureDebut->format('H:i:s') . " à " . $this->heureFin->format('H:i:s') .
            " avec un effort ressenti de " . $this->effortRessenti . "/5" .
            " et une température moyenne de " . $this->avrTemperature . "°C" .
            " et une variabilité de " . $this->variability . " bpm" .
            " et une variance de " . $this->variance . " bpm" .
            " et un écart type de " . $this->standardDeviation . " bpm" .
            " et une moyenne de " . $this->average . " bpm" .
            " et un maximum de " . $this->maximum . " bpm" .
            " et un minimum de " . $this->minimum . " bpm" .
            " et une pause automatique " . ($this->hasAutoPause ? "activée" : "désactivée") . ".\n";
    }
}
?>
