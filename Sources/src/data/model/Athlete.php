<?php
/**
 * Nom du fichier: Athlete.php
 *
 * @author PEREDERII Antoine
 * @date 2023-11-25
 * @description Classe Athlete représentant le rôle d'un athlète.
 *
 * @package model
 */

namespace Model;

use Shared\Exception\NotImplementedException;
use Stub\TrainingRepository;

/**
 * @class Athlete
 * @brief Classe représentant le rôle d'un athlète.
 */
class Athlete extends Role {
    // Attributs spécifiques à l'athlète si nécessaire
//    private array $statsList = [];
    private array $activityList = [];
//    private array $dataSourcesList = [];



    /**
     * Obtient la liste des activités de l'athlète.
     *
     * @return array La liste des activités de l'athlète.
     */
    public function getActivities(): array {
        return $this->activityList;
    }

    /**
     * Ajoute une activité à la liste des activités de l'athlète.
     *
     * @param Activity $myActivity L'activité à ajouter.
     * @return bool True si l'ajout est réussi, sinon false.
     */
    public function addActivity(Activity $myActivity): bool {
        if (isset($myActivity)) {
            $this->activityList[] = $myActivity;
            return true;
        }
        return false;
    }


    public function CheckAdd(User $user): bool
    {
        if($user->getRole() instanceof Athlete){
            return true;
        } else {
            return false;
        }
    }


}
?>