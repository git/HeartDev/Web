<?php

namespace Manager;

use Model\Coach;
use Model\Training;
use Network\IAuthService;

class CoachManager
{
    private IAuthService $authService;

    private DataManager $dataManager;

    public function __construct(DataManager $dataManager,IAuthService $authService)
    {
        $this->authService = $authService;
        $this->dataManager = $dataManager;

    }

    public function getUsersList(): ?array
    {
        if ($this->authService->getCurrentUser() && $this->authService->getCurrentUser()->getRole()->getUsersList()) {
            return $this->authService->getCurrentUser()->getRole()->getUsersList();
        }
        return null;
    }
    public function getTrainingsList(): ?array {
       return $this->dataManager->trainingRepository->getItems(0,10);
    }
    public function addUser(string $username): bool
    {
        if ($this->authService->getCurrentUser() && $this->authService->getCurrentUser()->getRole()) {
            $useToAdd = $this->dataManager->userRepository->getItemByName($username, 0, 1);
            if($useToAdd) { // count 1 seul et debuis 0 (debut)
                if ($this->authService->getCurrentUser()->getRole()->addUser($useToAdd)) {
                    return true;
                }
            }
        }
        return false;
    }
    public function removeUser(String $username): bool
    {
        if ($this->authService->getCurrentUser() && $this->authService->getCurrentUser()->getRole()) {
            if(($user = $this->dataManager->userRepository->getItemByName($username, 0, 1))) { // count 1 seul et debuis 0 (debut)
                if ($this->authService->getCurrentUser()->getRole()->removeUser($user)) {
                    return true;
                }
            }
        }
        return false;
    }
    public function addTraining(Training $training): bool
    {
        if ($this->authService->getCurrentUser() && $this->authService->getCurrentUser()->getRole() instanceof Coach) {

            if ($this->authService->getCurrentUser()->getRole()->addTraining($training)) {
                $this->dataManager->trainingRepository->addItem($training);
                return true;
            }
        }
        return false;
    }
    public function removeTraining(String $trainingId): bool
    {
        $index = array_search($trainingId, $this->authService->getCurrentUser()->getRole()->getTrainingsList());
        if ($index !== false) {
            unset($this->authService->getCurrentUser()->getRole()->getTrainingsList()[$index]);
            return true;
        }
        return false;
    }

   /* public function getStatistics(User $user) : ?Statistic {
        if ($this->authService->currentUser && $this->authService->currentUser->getRole()) {
            if (($arrayStat = $this->authService->currentUser->getRole()->getUserList($user)->getStatistic())) {
                return $arrayStat;
            }
        }
        return null;
    }

    public function getAnalyse(User $user): bool {
        return false;
    }*/

}