<?php
/**
 * Nom du fichier: AthleteManager.php
 *
 * @author PEREDERII Antoine
 * @date 2023-11-25
 * @description Classe AthleteManager pour gérer les opérations liées aux athlètes et à leurs activités.
 *
 * @package VotrePackage
 */
namespace Manager;

use Network\IAuthService;
use Stub\AuthService;

/**
 * @class AthleteManager
 * @brief Classe pour gérer les opérations liées aux athlètes.
 */
class AthleteManager {
    private IAuthService $authService;

    private DataManager $dataManager;
    /**
     * Constructeur de la classe AthleteManager.
     *
     * @param AuthService $authService Le service d'authentification utilisé pour vérifier l'utilisateur actuel.
     */
    public function __construct(DataManager $dataManager,IAuthService $authService)
    {
        $this->authService = $authService;
        $this->dataManager = $dataManager;
    }

    /**
     * Récupère les activités de l'athlète actuel.
     *
     * @return array|null Retourne un tableau d'objets Activity en cas de succès, sinon null.
     */
    public function getActivities(): ?array {
        if ($this->authService->getCurrentUser() && $this->authService->getCurrentUser()->getRole()->getActivities()) {
            return $this->authService->getCurrentUser()->getRole()->getActivities();
        }
        return null;
    }
}
?>
