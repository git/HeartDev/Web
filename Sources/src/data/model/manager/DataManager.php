<?php
namespace Manager;

use Repository\INotificationRepository;
use Repository\IRelationshipRequestRepository;
use Repository\ITrainingRepository;
use Repository\IUserRepository;

abstract class DataManager {
    public IUserRepository $userRepository;
    public IRelationshipRequestRepository $relationshipRequestRepository;
    public ITrainingRepository $trainingRepository;
    public INotificationRepository $notificationRepository;
}

?>