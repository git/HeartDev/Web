<?php

namespace Model;

interface Observer
{
    public function update(Observable $observable);
}
?>