<?php
/**
 * Nom du fichier: User.php
 *
 * @author PEREDERII Antoine
 * @date 2023-11-25
 * @description Classe représentant un utilisateur.
 *
 * @package model
 */

namespace Model;

/**
 * @class User
 * @brief Classe représentant un utilisateur.
 */
class User {
    private int $id;
    private String $username;
    private string $nom;
    private string $prenom;
    private string $email;
    private string $motDePasse;
    private string $sexe;
    private float $taille;
    private float $poids;
    private \DateTime $dateNaissance;
    private Role $role;
    protected array $notifications = [];

    /**
     * @return array
     */
    public function getNotifications(): array
    {
        return $this->notifications;
    }

    /**
     * @param int $id
     * @param string $nom
     * @param string $prenom
     * @param string $username
     * @param string $email
     * @param string $motDePasse
     * @param string $sexe
     * @param float $taille
     * @param float $poids
     * @param \DateTime $dateNaissance
     * @param \Model\Role $role
     */
    public function __construct(int $id, string $nom, string $prenom, string $username, string $email, string $motDePasse, string $sexe, float $taille, float $poids, \DateTime $dateNaissance, Role $role)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->username = $username;
        $this->email = $email;
        $this->motDePasse = $motDePasse;
        $this->sexe = $sexe;
        $this->taille = $taille;
        $this->poids = $poids;
        $this->dateNaissance = $dateNaissance;
        $this->role = $role;
    }

    public function addNotification($notification): void {
        $this->notifications[] = $notification;
    }

    // Method to delete a notification by its index
    public function deleteNotification($index): void {
        if (array_key_exists($index, $this->notifications)) {
            unset($this->notifications[$index]);
        }
    }
    /**
     * Obtient l'identifiant de l'utilisateur.
     *
     * @return int L'identifiant de l'utilisateur.
     */
    public function getId(): int {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * Définit l'identifiant de l'utilisateur.
     *
     * @param int $id Le nouvel identifiant de l'utilisateur.
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * Obtient le nom d'utilisateur de l'utilisateur.
     *
     * @return string Le nom d'utilisateur de l'utilisateur.
     */
    public function getNom(): string {
        return $this->nom;
    }

    /**
     * Définit le nom de l'utilisateur.
     *
     * @param string $nom Le nouveau nom de l'utilisateur.
     */
    public function setNom(string $nom): void {
        $this->nom = $nom;
    }

    /**
     * Obtient le prénom de l'utilisateur.
     *
     * @return string Le prénom de l'utilisateur.
     */
    public function getPrenom(): string {
        return $this->prenom;
    }

    /**
     * Définit le prénom de l'utilisateur.
     *
     * @param string $prenom Le nouveau prénom de l'utilisateur.
     */
    public function setPrenom(string $prenom): void {
        $this->prenom = $prenom;
    }

    /**
     * Obtient l'adresse e-mail de l'utilisateur.
     *
     * @return string L'adresse e-mail de l'utilisateur.
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * Définit l'adresse e-mail de l'utilisateur.
     *
     * @param string $email La nouvelle adresse e-mail de l'utilisateur.
     */
    public function setEmail(string $email): void {
        $this->email = $email;
    }

    /**
     * Obtient le mot de passe haché de l'utilisateur.
     *
     * @return string Le mot de passe haché de l'utilisateur.
     */
    public function getMotDePasse(): string {
        return $this->motDePasse;
    }

    /**
     * Définit le mot de passe haché de l'utilisateur.
     *
     * @param string $motDePasse Le nouveau mot de passe haché de l'utilisateur.
     */
    public function setMotDePasse(string $motDePasse): void {
        $this->motDePasse = $motDePasse;
    }

    /**
     * Obtient le sexe de l'utilisateur.
     *
     * @return string Le sexe de l'utilisateur.
     */
    public function getSexe(): string {
        return $this->sexe;
    }

    /**
     * Définit le sexe de l'utilisateur.
     *
     * @param string $sexe Le nouveau sexe de l'utilisateur.
     */
    public function setSexe(string $sexe): void {
        $this->sexe = $sexe;
    }

    /**
     * Obtient la taille de l'utilisateur.
     *
     * @return float La taille de l'utilisateur.
     */
    public function getTaille(): float {
        return $this->taille;
    }

    /**
     * Définit la taille de l'utilisateur.
     *
     * @param float $taille La nouvelle taille de l'utilisateur.
     */
    public function setTaille(float $taille): void {
        $this->taille = $taille;
    }

    /**
     * Obtient le poids de l'utilisateur.
     *
     * @return float Le poids de l'utilisateur.
     */
    public function getPoids(): float {
        return $this->poids;
    }

    /**
     * Définit le poids de l'utilisateur.
     *
     * @param float $poids Le nouveau poids de l'utilisateur.
     */
    public function setPoids(float $poids): void {
        $this->poids = $poids;
    }

    /**
     * Obtient la date de naissance de l'utilisateur.
     *
     * @return \DateTime La date de naissance de l'utilisateur.
     */
    public function getDateNaissance(): \DateTime {
        return $this->dateNaissance;
    }

    /**
     * Définit la date de naissance de l'utilisateur.
     *
     * @param \DateTime $dateNaissance La nouvelle date de naissance de l'utilisateur.
     */
    public function setDateNaissance(\DateTime $dateNaissance): void {
        $this->dateNaissance = $dateNaissance;
    }

    /**
     * Obtient le rôle de l'utilisateur.
     *
     * @return Role Le rôle de l'utilisateur.
     */
    public function getRole(): Role {
        return $this->role;
    }

    /**
     * Définit le rôle de l'utilisateur.
     *
     * @param Role $role Le nouveau rôle de l'utilisateur.
     */
    public function setRole(Role $role): void {
        $this->role = $role;
    }

    /**
     * Vérifie si le mot de passe fourni est valide.
     *
     * @param string $password Le mot de passe à vérifier.
     * @return bool True si le mot de passe est valide, sinon false.
     */
    public function isValidPassword(string $password): bool {
        // TODO: Implémenter la vérification du mot de passe (hachage).
        return $this->motDePasse === $password;
    }

    /**
     * Obtient une représentation textuelle de l'utilisateur.
     *
     * @return string La représentation textuelle de l'utilisateur.
     */
    public function __toString() {
        return "Athlete [ID: {$this->id}, Username : $this->username, Nom: {$this->nom}, Prénom: {$this->prenom}, Email: {$this->email}]";
    }
}
?>