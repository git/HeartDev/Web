<?php

namespace Model;

class Training
{
    private int $idTraining;
    private \DateTime $date;
    private float $latitude;
    private float $longitude;
    private ?String $description;
    private ?String $feedback;

    public function __construct(
        int $idTraining,
        \DateTime $date,
        float $latitude,
        float $longitude,
        ?String $description = null,
        ?String $feedback = null
    ) {
        $this->idTraining = $idTraining;
        $this->date = $date;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->description = $description;
        $this->feedback = $feedback;
    }
    public function getId():int {
        return $this->idTraining;
    }
    public function getDate():\DateTime {
        return $this->date;
    }
    public function getLocation(): String {
        return $this->longitude . $this->latitude;
    }
    public function getDescription(): String
    {
        return $this->description;
    }
    public function getFeedback(): String {
        return $this->feedback;
    }

    public function __toString(): String {
        return $this->idTraining . " - " . $this->description;
    }
}