<?php

namespace Model;

class RelationshipRequest extends Observable
{
    private int $id;
    private int $fromUser;
    private int $toUser;
    private string $status = ''; // 'pending', 'accepted', 'declined'
    private array $observers = [];

    // TODO maybe need to change do User or Tiny User or User DTO

    public function __construct(int $id, int $fromUser, int $toUser) {
        $this->id = $id;
        $this->fromUser = $fromUser;
        $this->toUser = $toUser;
    }

    public function updateStatus(string $newStatus): void {
        if (in_array($newStatus, ['pending', 'accepted', 'declined'])) {
            $this->status = $newStatus;

            $this->notifyObservers();
        } else {
            throw new \InvalidArgumentException("Invalid status: $newStatus");
        }
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getFromUser(): int
    {
        return $this->fromUser;
    }

    /**
     * @param int $fromUser
     */
    public function setFromUser(int $fromUser): void
    {
        $this->fromUser = $fromUser;
    }

    /**
     * @return int
     */
    public function getToUser(): int
    {
        return $this->toUser;
    }

    /**
     * @param int $toUser
     */
    public function setToUser(int $toUser): void
    {
        $this->toUser = $toUser;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    public function __toString(): string
    {
        return sprintf(
            "RelationshipRequest: ID: %d, FromUser: %d, ToUser: %d, Status: %s",
            $this->id,
            $this->fromUser,
            $this->toUser,
            $this->status
        );
    }

}