<?php 

    namespace Data\Core;

    class Preferences {
        private String $cookie;
        private Array $theme;

        public function __construct(){
            if (isset($_COOKIE['preferences'])){
                $this->cookie = $_COOKIE['preferences'];
            } else {
                $this->cookie = setcookie('preferences', 'base_theme', time()+(3600*24)*7);
            }
            $this->theme = array(
                'base_theme',
                'dark_theme',
                'pink_theme'
            );
        }

        public function majCookie(String $maj){
            try{
                foreach($this->theme as $t){
                        $this->cookie = $maj;
                        setcookie('preferences', $maj);
                }
            } catch (\Exception $e){
                throw new \ValueError;
            }
        }

        public function getCookie():String{
            return $this->cookie;
        }
    }

?>