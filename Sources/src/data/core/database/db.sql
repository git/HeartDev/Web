-- Athlete Table
CREATE TABLE Athlete (
    idAthlete SERIAL PRIMARY KEY,
    nom VARCHAR(255),
    prenom VARCHAR(255),
    email VARCHAR(255) UNIQUE,
    sexe CHAR(1),
    taille DECIMAL,
    poids DECIMAL,
    motDePasse VARCHAR(255),
    dateNaissance DATE
);

-- Friendship Table
CREATE TABLE Friendship (
    idAthlete1 INT,
    idAthlete2 INT,
    debut DATE,
    PRIMARY KEY (idAthlete1, idAthlete2),
    FOREIGN KEY (idAthlete1) REFERENCES Athlete (idAthlete),
    FOREIGN KEY (idAthlete2) REFERENCES Athlete (idAthlete)
);

-- Notification Table
CREATE TABLE Notification (
    idNotif SERIAL PRIMARY KEY,
    message TEXT,
    date DATE,
    statut BOOLEAN,
    urgence INT,
    athleteId INT,
    FOREIGN KEY (athleteId) REFERENCES Athlete (idAthlete)
);

-- Coach Table
CREATE TABLE Coach (
    idCoach SERIAL PRIMARY KEY,
    athleteId INT,
    FOREIGN KEY (athleteId) REFERENCES Athlete (idAthlete)
);

-- Statistique Table
CREATE TABLE Statistique (
    idStatistique SERIAL PRIMARY KEY,
    poids DECIMAL,
    fcMoyenne DECIMAL,
    fcMax DECIMAL,
    caloriesBruleesMoy DECIMAL,
    date DATE,
    athleteId INT,
    FOREIGN KEY (athleteId) REFERENCES Athlete (idAthlete)
);

-- Entrainement Table
CREATE TABLE Entrainement (
    idEntrainement SERIAL PRIMARY KEY,
    date DATE,
    description TEXT,
    latitude DECIMAL,
    longitude DECIMAL,
    feedback TEXT,
    coachId INT,
    FOREIGN KEY (coachId) REFERENCES Coach (idCoach)
);

-- Participe Table
CREATE TABLE Participe (
    athleteId INT,
    entrainementId INT,
    PRIMARY KEY (athleteId, entrainementId),
    FOREIGN KEY (athleteId) REFERENCES Athlete (idAthlete),
    FOREIGN KEY (entrainementId) REFERENCES Entrainement (idEntrainement)
);

-- SourceDonnee Table
CREATE TABLE SourceDonnee (
    idSource SERIAL PRIMARY KEY,
    type VARCHAR(255),
    modele VARCHAR(255),
    precision DECIMAL,
    athleteId INT,
    FOREIGN KEY (athleteId) REFERENCES Athlete (idAthlete)
);

-- Activite Table
CREATE TABLE Activite (
    idActivite SERIAL PRIMARY KEY,
    type VARCHAR(255),
    date DATE,
    heureDeDebut TIME,
    heureDeFin TIME,
    effortRessent DECIMAL,
    variabilite DECIMAL,
    variance DECIMAL,
    ecartType DECIMAL,
    moyenne DECIMAL,
    maximum DECIMAL,
    minimum DECIMAL,
    temperatureMoyenne DECIMAL,
    athleteId INT,
    sourceId INT,
    FOREIGN KEY (athleteId) REFERENCES Athlete (idAthlete),
    FOREIGN KEY (sourceId) REFERENCES SourceDonnee (idSource)
);

-- FrequenceCardiaque Table
CREATE TABLE FrequenceCardiaque (
    idFc SERIAL PRIMARY KEY,
    altitude DECIMAL,
    temps TIME,
    temperature DECIMAL,
    bpm INT,
    longitude DECIMAL,
    latitude DECIMAL,
    activiteId INT,
    FOREIGN KEY (activiteId) REFERENCES Activite (idActivite)
);

-- Insertion de données dans la table Athlete
INSERT INTO Athlete (nom, prenom, email, sexe, taille, poids, motDePasse, dateNaissance) VALUES
('Doe', 'John', 'john.doe@example.com', 'M', 1.80, 70, 'password123', '1990-01-01'),
('Smith', 'Jane', 'jane.smith@example.com', 'F', 1.65, 60, 'password456', '1992-02-02');

-- Insertion de données dans la table Friendship
INSERT INTO Friendship (idAthlete1, idAthlete2, debut) VALUES
(1, 2, '2023-01-01');

-- Insertion de données dans la table Notification
INSERT INTO Notification (message, date, statut, urgence, athleteId) VALUES
('Training session at 10 AM', '2023-03-10', TRUE, 1, 1);

-- Insertion de données dans la table Coach
INSERT INTO Coach (athleteId) VALUES
(1);

-- Insertion de données dans la table Statistique
INSERT INTO Statistique (poids, fcMoyenne, fcMax, caloriesBruleesMoy, date, athleteId) VALUES
(70, 80, 150, 500, '2023-03-10', 1);

-- Insertion de données dans la table Entrainement
INSERT INTO Entrainement (date, description, latitude, longitude, feedback, coachId) VALUES
('2023-03-12', 'Long run in the park', 40.7128, -74.0060, 'Good effort', 1);

-- Insertion de données dans la table Participe
INSERT INTO Participe (athleteId, entrainementId) VALUES
(1, 1);

-- Insertion de données dans la table SourceDonnee
INSERT INTO SourceDonnee (type, modele, precision, athleteId) VALUES
('Heart Rate Monitor', 'HRM-Pro', 98.5, 1);

-- Insertion de données dans la table Activite
INSERT INTO Activite (type, date, heureDeDebut, heureDeFin, effortRessent, variabilite, variance, ecartType, moyenne, maximum, minimum, temperatureMoyenne, athleteId, sourceId) VALUES
('Running', '2023-03-10', '08:00:00', '09:00:00', 7, 0.5, 1, 0.1, 140, 160, 120, 20, 1, 1);

-- Insertion de données dans la table FrequenceCardiaque
INSERT INTO FrequenceCardiaque (altitude, temps, temperature, bpm, longitude, latitude, activiteId) VALUES
(100, '08:15:00', 15, 130, -74.0060, 40.7128, 1);
