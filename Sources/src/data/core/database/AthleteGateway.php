<?php

namespace Database;
use \PDO;

class AthleteGateway {
    private $connection;

    public function __construct(Connexion $connection) {
        $this->connection = $connection;
    }

    public function getAthlete() {
        $query = "SELECT * FROM Athlete";
        return $this->connection->executeWithErrorHandling($query);
    }

    public function getAthleteById(int $userId) {
        $query = "SELECT * FROM Athlete WHERE idAthlete = :id";
        $params = [':id' => [$userId, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByName(string $name) {
        $query = "SELECT * FROM Athlete WHERE nom = :name";
        $params = [':name' => [$name, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByFirstName(string $firstName) {
        $query = "SELECT * FROM Athlete WHERE prenom = :firstName";
        $params = [':firstName' => [$firstName, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByEmail(string $email) {
        $query = "SELECT * FROM Athlete WHERE email = :email";
        $params = [':email' => [$email, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByGender(string $gender) {
        $query = "SELECT * FROM Athlete WHERE sexe = :gender";
        $params = [':gender' => [$gender, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByHeight(int $height) {
        $query = "SELECT * FROM Athlete WHERE taille = :height";
        $params = [':height' => [$height, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByWeight(int $weight) {
        $query = "SELECT * FROM Athlete WHERE poids = :weight";
        $params = [':weight' => [$weight, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getAthleteByBirthDate(string $birthdate) {
        $query = "SELECT * FROM Athlete WHERE dateNaissance = :birthdate";
        $params = [':birthdate' => [$birthdate, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function addAthlete(AthleteEntity $athlete) {
        $query = "INSERT INTO Athlete (nom, prenom, email, sexe, taille, poids, motDePasse, dateNaissance) 
                  VALUES (:nom, :prenom, :email, :sexe, :taille, :poids, :motDePasse, :dateNaissance)";

        $params = [
            ':nom' => $athlete->getNom(),
            ':prenom' => $athlete->getPrenom(),
            ':email' => $athlete->getEmail(),
            ':sexe' => $athlete->getSexe(),
            ':taille' => $athlete->getTaille(),
            ':poids' => $athlete->getPoids(),
            ':motDePasse' => $athlete->getMotDePasse(),
            ':dateNaissance' => $athlete->getDateNaissance(), 
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function updateAthlete(AthleteEntity $oldAthlete, AthleteEntity $newAthlete) {
        $query = "UPDATE Athlete 
                  SET nom = :nom, prenom = :prenom, email = :email, sexe = :sexe,
                  taille = :taille, poids = :poids, motDePasse = :motDePasse, dateNaissance = :dateNaissance
                  WHERE idAthlete = :idAthlete";

        $params = [
            ':idAthlete' => $oldAthlete->getIdAthlete(),
            ':nom' => $newAthlete->getNom(),
            ':prenom' => $newAthlete->getPrenom(),
            ':email' => $newAthlete->getEmail(),
            ':sexe' => $newAthlete->getSexe(),
            ':taille' => $newAthlete->getTaille(),
            ':poids' => $newAthlete->getPoids(),
            ':motDePasse' => $newAthlete->getMotDePasse(),
            ':dateNaissance' => $newAthlete->getDateNaissance(), 
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function deleteAthlete(int $idAthlete) {
        $query = "DELETE FROM Athlete WHERE idAthlete = :idAthlete";

        $params = [
            ':idAthlete' => $idAthlete,
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }



}


// Exemple d'utilisation
//$dsn = "pgsql:host=localhost;port=5432;dbname=mydatabase;user=myuser;password=mypassword";
//$connection = new Connection($dsn);
//$gateway = new AthleteGateway($connection);

//$allAth = $gateway->getAthlete();
//print_r($allAth);

//$singleAth = $gateway->getAthleteById(1);
//print_r($singleAth);

?>
