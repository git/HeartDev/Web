<?php

namespace Database;

class CoachEntity {
    private $idCoach;
    private $nom;
    private $prenom;
    private $email;
    private $sexe;
    private $taille;
    private $poids;
    private $motDePasse;
    private $dateNaissance;

    // Getters
    public function getIdCoach() {
        return $this->idCoach;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getSexe() {
        return $this->sexe;
    }

    public function getTaille() {
        return $this->taille;
    }

    public function getPoids() {
        return $this->poids;
    }

    public function getMotDePasse() {
        return $this->motDePasse;
    }

    public function getDateNaissance() {
        return $this->dateNaissance;
    }

    // Setters
    public function setIdCoach($idCoach) {
        $this->idCoach = $idCoach;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setSexe($sexe) {
        $this->sexe = $sexe;
    }

    public function setTaille($taille) {
        $this->taille = $taille;
    }

    public function setPoids($poids) {
        $this->poids = $poids;
    }

    public function setMotDePasse($motDePasse) {
        $this->motDePasse = $motDePasse;
    }

    public function setDateNaissance($dateNaissance) {
        $this->dateNaissance = $dateNaissance;
    }
}

?>
