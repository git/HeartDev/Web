<?php

namespace Database;
use \PDO;

class CoachGateway {
    private $connection;

    public function __construct(Connexion $connection) {
        $this->connection = $connection;
    }

    public function getCoach() {
        $query = "SELECT * FROM Coach";
        return $this->connection->executeWithErrorHandling($query);
    }

    public function getCoachById(int $userId) {
        $query = "SELECT * FROM Coach WHERE idCoach = :id";
        $params = [':id' => [$userId, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getCoachByName(string $name) {
        $query = "SELECT * FROM Coach WHERE nom = :name";
        $params = [':name' => [$name, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getCoachByFirstName(string $firstName) {
        $query = "SELECT * FROM Coach WHERE prenom = :firstName";
        $params = [':firstName' => [$firstName, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getCoachByEmail(string $email) {
        $query = "SELECT * FROM Coach WHERE email = :email";
        $params = [':email' => [$email, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getCoachByGender(string $gender) {
        $query = "SELECT * FROM Coach WHERE sexe = :gender";
        $params = [':gender' => [$gender, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getCoachByHeight(int $height) {
        $query = "SELECT * FROM Coach WHERE taille = :height";
        $params = [':height' => [$height, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getCoachByBirthDate(string $birthdate) {
        $query = "SELECT * FROM Coach WHERE dateNaissance = :birthdate";
        $params = [':birthdate' => [$birthdate, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function addCoach(CoachEntity $coach) {
        $query = "INSERT INTO Coach (nom, prenom, email, sexe, taille, poids, motDePasse, dateNaissance) 
                  VALUES (:nom, :prenom, :email, :sexe, :taille, :poids, :motDePasse, :dateNaissance)";

        $params = [
            ':nom' => $coach->getNom(),
            ':prenom' => $coach->getPrenom(),
            ':email' => $coach->getEmail(),
            ':sexe' => $coach->getSexe(),
            ':taille' => $coach->getTaille(),
            ':poids' => $coach->getPoids(),
            ':motDePasse' => $coach->getMotDePasse(),
            ':dateNaissance' => $coach->getDateNaissance(), 
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function updateCoach(CoachEntity $oldCoach, CoachEntity $newCoach) {
        $query = "UPDATE Coach 
                  SET nom = :nom, prenom = :prenom, email = :email, sexe = :sexe,
                  taille = :taille, poids = :poids, motDePasse = :motDePasse, dateNaissance = :dateNaissance
                  WHERE idCoach = :idCoach";

        $params = [
            ':idCoach' => $oldCoach->getIdCoach(),
            ':nom' => $newCoach->getNom(),
            ':prenom' => $newCoach->getPrenom(),
            ':email' => $newCoach->getEmail(),
            ':sexe' => $newCoach->getSexe(),
            ':taille' => $newCoach->getTaille(),
            ':poids' => $newCoach->getPoids(),
            ':motDePasse' => $newCoach->getMotDePasse(),
            ':dateNaissance' => $newCoach->getDateNaissance(), 
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function deleteCoach(int $idCoach) {
        $query = "DELETE FROM Coach WHERE idCoach = :idCoach";

        $params = [
            ':idCoach' => $idCoach,
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

}