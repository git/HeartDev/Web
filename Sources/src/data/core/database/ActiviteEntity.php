<?php

namespace Database;

class ActiviteEntity {
    private $idActivite;
    private $type;
    private $date;
    private $heureDebut;
    private $heureFin;
    private $effortRessenti;
    private $variabilite;
    private $variance;
    private $ecartType;
    private $moyenne;
    private $maximum;
    private $minimum;
    private $temperatureMoyenne;

    // Getters
    public function getIdActivite() {
        return $this->idActivity;
    }

    public function getType() {
        return $this->type;
    }

    public function getDate() {
        return $this->date;
    }

    public function getHeureDebut() {
        return $this->heureDebut;
    }

    public function getHeureFin() {
        return $this->heureFin;
    }

    public function getEffortRessenti() {
        return $this->effortRessenti;
    }

    public function getVariabilite() {
        return $this->variabilite;
    }

    public function getVariance() {
        return $this->variance;
    }

    public function getEcartType() {
        return $this->ecartType;
    }

    public function getMoyenne() {
        return $this->moyenne;
    }

    public function getMaximum() {
        return $this->maximum;
    }

    public function getMinimum() {
        return $this->minimum;
    }

    public function getTemperatureMoyenne() {
        return $this->temperatureMoyenne;
    }

    // Setters
    public function setIdActivite($idActivite) {
        $this->idActivity = $idActivity;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    public function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }

    public function setEffortRessenti($effortRessenti) {
        $this->effortRessenti = $effortRessenti;
    }

    public function setVariabilite($variabilite) {
        $this->variabilite = $variabilite;
    }

    public function setVariance($variance) {
        $this->variance = $variance;
    }

    public function setEcartType($ecartType) {
        $this->ecartType = $ecartType;
    }

    public function setMoyenne($moyenne) {
        $this->moyenne = $moyenne;
    }

    public function setMaximum($maximum) {
        $this->maximum = $maximum;
    }

    public function setMinimum($minimum) {
        $this->minimum = $minimum;
    }

    public function setTemperatureMoyenne($temperatureMoyenne) {
        $this->temperatureMoyenne = $temperatureMoyenne;
    }
}

?>
