<?php

namespace Database;
use Model\Activite;

class ActiviteMapper {
    public function map(array $data):ActiviteEntity {
        $activite = new ActiviteEntity();
        $activite->setIdActivite($data['idActivite']);
        $activite->setType($data['type']);
        $activite->setDate($data['date']);
        $activite->setHeureDebut($data['heureDebut']);
        $activite->setHeureFin($data['heureFin']);
        $activite->setEffortRessenti($data['effortRessenti']);
        $activite->setVariabilite($data['variabilite']);
        $activite->setVariance($data['variance']);
        $activite->setEcartType($data['ecartType']);
        $activite->setMoyenne($data['moyenne']);
        $activite->setMaximum($data['maximum']);
        $activite->setMinimum($data['minimum']);
        $activite->setTemperatureMoyenne($data['temperatureMoyenne']);

        return $activite;
    }

    //public function ActiviteEntityToModel(ActiviteEntity entity): Activite;

    public function ActiviteEntityToModel(ActiviteEntity $activiteEntity):Activite{

        $act = new Activite(
            $activiteEntity->getIdActivite(),
            $activiteEntity->getType(),
            $activiteEntity->getDate(),
            $activiteEntity->getHeureDebut(),
            $activiteEntity->getHeureFin(),
            $activiteEntity->getEffortRessenti(),
            $activiteEntity->getVariabilite(),
            $activiteEntity->getVariance(),
            $activiteEntity->getEcartType(),
            $activiteEntity->getMoyenne(),
            $activiteEntity->getMaximum(),
            $activiteEntity->getMinimum(),
            $activiteEntity->getTemperatureMoyenne()
        );

        return $act;
    }
    //public function ActiviteToEntity(Activite model): ActiviteEntity;
}

?>
