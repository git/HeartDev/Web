<?php

namespace Database;
use Model\User;
use \PDO;
use \DateTime;
use Model\Role;
use Model\Athlete;

class AthleteMapper {
    public function fromSqlToEntity(array $data): array {
        $athleteEntities = [];

        foreach ($data as $athleteData) {
            $athlete = new AthleteEntity();

            if (isset($athleteData['idAthlete'])) {
                $athlete->setIdAthlete($athleteData['idAthlete']);
            }

            if (isset($athleteData['nom'])) {
                $athlete->setNom($athleteData['nom']);
            }

            if (isset($athleteData['prenom'])) {
                $athlete->setPrenom($athleteData['prenom']);
            }

            if (isset($athleteData['email'])) {
                $athlete->setEmail($athleteData['email']);
            }

            if (isset($athleteData['sexe'])) {
                $athlete->setSexe($athleteData['sexe']);
            }

            if (isset($athleteData['taille'])) {
                $athlete->setTaille($athleteData['taille']);
            }

            if (isset($athleteData['poids'])) {
                $athlete->setPoids($athleteData['poids']);
            }

            if (isset($athleteData['motDePasse'])) {
                $athlete->setMotDePasse($athleteData['motDePasse']);
            }

            if (isset($athleteData['dateNaissance'])) {
                $athlete->setDateNaissance($athleteData['dateNaissance']);
            }

            $athleteEntities[] = $athlete;
        }

        return $athleteEntities;
    }
    
    public function athleteEntityToModel(AthleteEntity $athleteEntity): User {
        $role = new Athlete(); // Utilisez la classe Athlete

        $dateSpecifique = $athleteEntity->getDateNaissance();
        $date = new DateTime($dateSpecifique);

        $user = new User(
            $athleteEntity->getIdAthlete(),
            $athleteEntity->getNom(),
            $athleteEntity->getPrenom(),
            $athleteEntity->getEmail(),
            $athleteEntity->getMotDePasse(),
            $athleteEntity->getSexe(),
            $athleteEntity->getTaille(),
            $athleteEntity->getPoids(),
            $date,
            $role
        );

        return $user;
    }

    public function athleteToEntity(User $user):AthleteEntity{

        $ath = new AthleteEntity();
        $ath->setIdAthlete($user->getId());
        $ath->setNom($user->getNom());
        $ath->setPrenom($user->getPrenom());
        $ath->setEmail($user->getEmail());
        $ath->setSexe($user->getSexe());
        $ath->setTaille($user->getTaille());
        $ath->setPoids($user->getPoids());
        $ath->setMotDePasse($user->getMotDePasse());
        $ath->setDateNaissance($user->getDateNaissance());

        return $ath;
    }

}

?>
