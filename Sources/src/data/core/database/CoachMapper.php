<?php

namespace Database;
use Model\User;
use \PDO;
use \DateTime;
use Model\Role;
use Model\Coach;

class CoachMapper {
    public function map(array $data) {
        $coach = new CoachEntity();
        $coach->setIdCoach($data['idCoach']);
        $coach->setNom($data['nom']);
        $coach->setPrenom($data['prenom']);
        $coach->setEmail($data['email']);
        $coach->setSexe($data['sexe']);
        $coach->setTaille($data['taille']);
        $coach->setPoids($data['poids']);
        $coach->setMotDePasse($data['motDePasse']);
        $coach->setDateNaissance($data['dateNaissance']);
        
        return $coach;
    }

    public function CoachEntityToModel(CoachEntity $coachEntity):User{
        $role = "Coach";

        $user = new User(
            $coachEntity->getIdCoach(),
            $coachEntity->getNom(),
            $coachEntity->getPrenom(),
            $coachEntity->getEmail(),
            $coachEntity->getMotDePasse(),
            $coachEntity->getSexe(),
            $coachEntity->getTaille(),
            $coachEntity->getPoids(),
            $coachEntity->getDateNaissance(),
            $role
        );

        return $user;
    }

    public function CoachToEntity(User $user):CoachEntity{

        $coach = new CoachEntity();
        $coach->setIdCoach($user->getId());
        $coach->setNom($user->getNom());
        $coach->setPrenom($user->getPrenom());
        $coach->setEmail($user->getEmail());
        $coach->setSexe($user->getSexe());
        $coach->setTaille($user->getTaille());
        $coach->setPoids($user->getPoids());
        $coach->setMotDePasse($user->getMotDePasse());
        $coach->setDateNaissance($user->getDateNaissance());

        return $coach;
    }
}

?>
