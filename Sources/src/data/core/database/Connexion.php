<?php

namespace Database;

class Connexion extends \PDO {
    private $stmt;

    public function __construct(string $dsn,string $username, string $password) {
        try {
            parent::__construct($dsn,$username,$password);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            // Log error or handle it as needed
            throw new \PDOException("Error connecting to the database: " . $e->getMessage());
        }
    }

    public function executeQuery(string $query, array $parameters = []): bool {
        $this->stmt = $this->prepare($query);
        //foreach ($parameters as $name => $value) {
        //    $this->stmt->bindValue($name, $value[0], $value[1]);
        //}
        foreach ($parameters as $name => $value) {
            $bindValueResult = $this->stmt->bindValue($name, $value, \PDO::PARAM_STR); 
            if (!$bindValueResult) {
                // Gérez l'erreur, par exemple, en lançant une exception.
                throw new \PDOException('Failed to bind value for parameter ' . $name);
            }
        }
        return $this->stmt->execute();
    }

    public function executeWithErrorHandling(string $query, array $params = []): array {
        try {
            $this->beginTransaction();
            $this->executeQuery($query, $params);
            $this->commit();
            return $this->getResults();
        } catch (\PDOException $e) {
            $this->rollBack();
            throw new \PDOException('Unexpected error on database client: ' . $e->getMessage());
        }
    }

    public function getResults(): array {
        return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
?>