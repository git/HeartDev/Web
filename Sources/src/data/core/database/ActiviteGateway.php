<?php

namespace Database;

class ActiviteGateway {
    private $connection;

    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }

    public function getActivite() {
        $query = "SELECT * FROM Activite";
        return $this->connection->executeWithErrorHandling($query);
    }

    public function getActiviteById(int $activiteId) {
        $query = "SELECT * FROM Activite WHERE idActivite = :id";
        $params = [':id' => [$activiteId, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getActiviteByType(string $type) {
        $query = "SELECT * FROM Activite WHERE type = :type";
        $params = [':type' => [$type, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getActiviteByDate(string $date) {
        $query = "SELECT * FROM Activite WHERE date = :date";
        $params = [':date' => [$date, PDO::PARAM_STR]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getActiviteByTimeRange(string $startTime, string $endTime) {
        $query = "SELECT * FROM Activite WHERE heureDebut >= :startTime AND heureFin <= :endTime";
        $params = [
            ':startTime' => [$startTime, PDO::PARAM_STR],
            ':endTime' => [$endTime, PDO::PARAM_STR]
        ];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getActiviteByEffort(int $effortRessenti) {
        $query = "SELECT * FROM Activite WHERE effortRessenti = :effort";
        $params = [':effort' => [$effortRessenti, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getActiviteByVariability(int $variabilite) {
        $query = "SELECT * FROM Activite WHERE variabilite = :variability";
        $params = [':variability' => [$variabilite, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function getActiviteByTemperature(int $temperatureMoyenne) {
        $query = "SELECT * FROM Activite WHERE temperatureMoyenne = :temperature";
        $params = [':temperature' => [$temperatureMoyenne, PDO::PARAM_INT]];
        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function addActivite(ActiviteEntity $activite) {
        $query = "INSERT INTO Activite (type, date, heureDebut, heureDeFin, effortRessenti, variabilite, variance, ecartType, moyenne, maximum, minimum, temperatureMoyenne) 
                  VALUES (:type, :date, :heureDebut, :heureDeFin, :effortRessenti, :variabilite, :variance, :ecartType, :moyenne, :maximum, :minimum, :temperatureMoyenne)";

        $params = [
            ':type' => $activite->getType(),
            ':date' => $activite->getDate()->format('Y-m-d'), // Format date pour SQL
            ':heureDebut' => $activite->getHeureDebut()->format('H:i:s'), // Format heure pour SQL
            ':heureDeFin' => $activite->getHeureFin()->format('H:i:s'), // Format heure pour SQL
            ':effortRessenti' => $activite->getEffortRessenti(),
            ':variabilite' => $activite->getVariabilite(),
            ':variance' => $activite->getVariance(),
            ':ecartType' => $activite->getEcartType(),
            ':moyenne' => $activite->getMoyenne(),
            ':maximum' => $activite->getMaximum(),
            ':minimum' => $activite->getMinimum(),
            ':temperatureMoyenne' => $activite->getTemperatureMoyenne(),
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function updateActivite(ActiviteEntity $oldActivite, ActiviteEntity $newActivite) {
        $query = "UPDATE Activite
                  SET type = :type, date = :date, heureDebut = :heureDebut, heureDeFin = :heureDeFin,
                  effortRessenti = :effortRessenti, variabilite = :variabilite, variance = :variance, ecartType = :ecartType, moyenne = :moyenne, maximum = :maximum, minimum = :minimum, temperatureMoyenne = :temperatureMoyenne
                  WHERE idActivite = :idActivite";

        $params = [
            ':idActivite' => $oldActivite->getIdActivite(),
            ':type' => $newActivite->getType(),
            ':date' => $newActivite->getDate()->format('Y-m-d'), // Format date pour SQL
            ':heureDebut' => $newActivite->getHeureDebut()->format('H:i:s'), // Format heure pour SQL
            ':heureDeFin' => $newActivite->getHeureFin()->format('H:i:s'), // Format heure pour SQL
            ':effortRessenti' => $newActivite->getEffortRessenti(),
            ':variabilite' => $newActivite->getVariabilite(),
            ':variance' => $newActivite->getVariance(),
            ':ecartType' => $newActivite->getEcartType(),
            ':moyenne' => $newActivite->getMoyenne(),
            ':maximum' => $newActivite->getMaximum(),
            ':minimum' => $newActivite->getMinimum(),
            ':temperatureMoyenne' => $newActivite->getTemperatureMoyenne(),
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }

    public function deleteActivite(int $idActivite) {
        $query = "DELETE FROM Activite WHERE idActivite = :idActivite";

        $params = [
            ':idActivite' => $idActivite,
        ];

        return $this->connection->executeWithErrorHandling($query, $params);
    }
}

?>
