<?php
namespace Network;

use Model\Notification;

interface INotificationService extends \Model\Observer {
    public function sendNotification(int $toUserId,Notification $notification);
}