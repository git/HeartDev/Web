<?php
namespace Network;
use Model\User;

/**
 *  Interface IAuthService
 *  Adding more methods here may violate the Single Responsibility Principle (SRP).
 *  It's recommended to keep this interface focused on authentication-related functionality.
*/
interface IAuthService {

    /**
     * Get the currently authenticated user.
     *
     * This method returns an instance of the User model representing
     * the currently authenticated user. If no user is authenticated,
     * the behavior of this method is implementation-dependent: it could return null,
     * throw an exception, etc.
     *
     * @return User|null The currently authenticated user, or null if no user is authenticated.
     */
    public function getCurrentUser(): ?User;
    /**
     * Authenticate a user.
     *
     * @param string $emailUser The emailUser of the user.
     * @param string $password The password of the user.
     *
     * @return ?User True if authentication is successful, false otherwise.
     */
    public function login(string $emailUser, string $password): bool;

     /**
     * Register a new user.
     *
     * @param string $username The username of the new user.
     * @param string $password The password of the new user.
     * @param $data other data {undefined} for the moment.
     *
     * @return bool True if registration is successful, false otherwise.
     */
    public function register(string $username, string $password, $data): bool;

    /**
     * Logout the currently authenticated user.
     * 
     * @return void
     */
    public function logoutUser(): bool;

}