<?php

namespace Network;

use Model\RelationshipRequest;
use Model\User;


interface IFriendRequestService {
    public function sendRequest(User $fromUser, User $toUser);
    public function acceptRequest(RelationshipRequest $request);
    public function declineRequest(RelationshipRequest $request);
}