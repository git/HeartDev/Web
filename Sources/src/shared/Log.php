<?php
namespace Shared;

class Log {
    const INFO = 'INFO';
    const WARNING = 'WARNING';
    const ERROR = 'ERROR';

    private static int $logCount = 0;
    private static array $logs = [];

    private static function getLogStyle(string $level): string {
        return match ($level) {
            self::WARNING => "background-color: #fff3cd; color: #856404; border-color: #ffeeba",
            self::ERROR => "background-color: #f8d7da; color: #721c24; border-color: #f5c6cb",
            default => "background-color: #f8f8f8; color: #383d41; border-color: #ccc",
        };
    }

    private static function addLog($something, string $level, string $label): bool|string
    {
        $style = self::getLogStyle($level);

        ob_start();
        echo "<pre style='$style; padding: 10px;'>";
        echo "<strong>$label [$level] (Log " . self::$logCount . ")</strong><br>";
        var_dump($something);
        echo "</pre>";
        $logOutput = ob_get_clean();

        self::$logs[] = ['level' => $level, 'output' => $logOutput];
        return $logOutput;
    }

    public static function dd($something, string $label = "LOGGER", string $level = self::INFO) {
        echo self::addLog($something, $level, $label);
        die();
    }

    public static function ddArray(array $array, string $label = "LOGGER", string $level = self::INFO) {
        self::dd($array, $label, $level);
    }

    public static function log($something, string $label = "LOGGER", string $level = self::INFO) {
        self::$logCount++;
        echo self::addLog($something, $level, $label);
    }

    public static function getLogCount(): int
    {
        return self::$logCount;
    }

    public static function getLogsByLevel(string $level): array
    {
        return array_filter(self::$logs, function($log) use ($level) {
            return $log['level'] === $level;
        });
    }
}
