<?php
namespace Shared;
use \Exception;
final class Validation {

    /**
     * Valide une action passée en paramètre.
     * 
     * @param string $action L'action à valider.
     * @return string L'action validée si elle est conforme.
     * @throws Exception Si l'action n'est pas valide.
     */
    public static function val_action($action) {
        if (!isset($action) || !Validation::val_string($action)) {
            throw new Exception("Pas d'action spécifiée.");
        } else {
            return $action;
        }
    }

    /**
     * Valide une chaîne de caractères.
     * 
     * @param string $string La chaîne à valider.
     * @return bool True si la chaîne est valide, sinon false.
     * @throws Exception Si la chaîne n'est pas valide (tentative d'injection de code).
     */
    public static function val_string(string $string) : bool {
        if (strlen(htmlspecialchars($string, ENT_QUOTES) === 0)) {
            throw new Exception("$string n'est pas valide. Tentative d'injection de code (attaque sécurité)!");
        } else {
            return true;
        }
    }

    /**
     * Valide un entier.
     * 
     * @param int $int L'entier à valider.
     * @return bool True si l'entier est valide, sinon false.
     * @throws Exception Si l'entier n'est pas valide (tentative d'injection de code).
     */
    public static function val_int(int $int) : bool {
        if (filter_var($int, FILTER_SANITIZE_NUMBER_INT) !== $int) {
            throw new Exception("$int n'est pas valide. Tentative d'injection de code (attaque sécurité)!");
        } else {
            return true;
        }
    }

    /**
     * Valide un mot de passe.
     * 
     * @param string $password Le mot de passe à valider.
     * @return bool True si le mot de passe est valide, sinon false.
     * @throws Exception Si le mot de passe n'est pas valide.
     */
    public static function val_password(string $password) : bool {
        if (!isset($password)) {
            throw new Exception("Le mot de passe ne peut être vide.");
        } else {
            if (!preg_match('/^.{6,}$/', $password)) {
                throw new Exception("Le mot de passe n'est pas valide : au moins 6 caractères requis.");
            }
            return Validation::val_string($password);
        }
    }
    /**
     * Valide un mail.
     *
     * @param string $email Le mail à valider.
     * @return bool True si le mail est valide, sinon false.
     * @throws Exception Si le mail n'est pas valide.
     */
    public static function val_email(string $email) : bool {
        if (!isset($email)) {
            throw new Exception("L'adresse e-mail ne peut être vide.");
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new Exception("L'adresse e-mail n'est pas valide.");
            }
            return true;
        }
    }


    /**
     * Valide un booléen.
     * 
     * @param bool $done La valeur booléenne à valider.
     * @return bool True si la valeur booléenne est définie, sinon false.
     */
    public static function val_bool(bool $done) : bool {
        return isset($done);
    }

    /**
     * Nettoie une chaîne de caractères.
     * 
     * @param string $string La chaîne à nettoyer.
     * @return string La chaîne nettoyée.
     */
    public static function clean_string(string $string) : string {
        return filter_var($string, FILTER_SANITIZE_STRING);
    }

    /**
     * Nettoie un entier.
     * 
     * @param int $int L'entier à nettoyer.
     * @return int L'entier nettoyé.
     */
    public static function clean_int(int $int) : int {
        return filter_var($int, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Nettoie une valeur booléenne.
     * 
     * @param bool $bool La valeur booléenne à nettoyer.
     * @return bool La valeur booléenne nettoyée.
     */
    public static function clean_bool(bool $bool) : bool {
        return filter_var($bool, FILTER_VALIDATE_BOOLEAN);
    }

}

?>