<?php

namespace Shared\Exception;

final class NotImplementedException extends \Exception
{
    public $message = "Not implemented method call";
}
