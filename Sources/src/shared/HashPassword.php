<?php
namespace Shared;

class HashPassword implements IHashPassword{
    public const MAX_PASSWORD_LENGTH = 4096;
    public function hashPassword(string $rawPassword): string
    {
        if (empty($rawPassword)) {
            throw new \InvalidArgumentException("Password to hash is invalid");
        }
        if (strlen($rawPassword) > self::MAX_PASSWORD_LENGTH)
        {
            throw new \InvalidArgumentException("Password too long");
        }

        // Generate a password hash using a secure algorithm
        $hash = password_hash($rawPassword, PASSWORD_DEFAULT);

        if ($hash === false) {
            throw new \RuntimeException("Password hashing failed.");
        }

        return $hash;
    }

    public function isPasswordValid(string $hashedPassword, string $rawPassword): bool
    {
        if (empty($hashedPassword) || empty($rawPassword)) {
            throw new \InvalidArgumentException("Encoded password or raw password is invalid");
        }

        // Use password_verify to check if the raw password matches the encoded one
        return password_verify($rawPassword, $hashedPassword);
    }
}
?>