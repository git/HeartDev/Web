<?php

namespace Shared\Attributes;

#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::TARGET_CLASS)]
class Route
{

    public function __construct(
        private string|array $path = '',
        private ?string $name = null,
        private array|string $methods = ['GET'],
    )
    {}

    public function getPath(): array| string{
        return $this->path;
    }

    public function getName(): ?string{
        return $this->name;
    }
    public function getMethods(): array | string
    {
        return $this->methods;
    }


}

