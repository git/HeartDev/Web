<?php
namespace Shared;

use App\Router\Request\IRequest;

/**
 * Interface for classes that resolve arguments for controller methods.
 */
interface IArgumentResolver
{
    /**
     * Resolves the arguments for a controller method based on the given request.
     *
     * @param IRequest $request    The request object.
     * @param callable $controller The controller callable.
     * @return array               An array of arguments resolved for the controller method.
     */
    public function getArguments(IRequest $request, callable $controller): array;
}
