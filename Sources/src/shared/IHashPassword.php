<?php
namespace Shared;

interface IHashPassword
{
    /**
     * Hash the raw password.
     *
     * @return string The hashed password
     *
     * @throws \InvalidArgumentException If the password to hash is invalid
     */
    public function hashPassword(string $raw) : string ;

    /**
     * Checks a raw password against an hash password.
     *
     * @param string      $hashed An hash password
     * @param string      $raw     A raw password
     *
     * @return bool true if the password is valid, false otherwise
     *
     * @throws \InvalidArgumentException If the salt is invalid
     */
    public function isPasswordValid(string $hashed, string $raw) : bool;

}

?>