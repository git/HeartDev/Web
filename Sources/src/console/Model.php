<?php

namespace Console;

use Manager\ActivityManager;
use Manager\AthleteManager;
use Manager\CoachManager;
use Manager\DataManager;
use Manager\UserManager;
use Network\RelationshipService;
use Stub\NotificationService;
use Stub\StubData;

class Model
{
    public UserManager $userMgr;
    public CoachManager $coachMgr;
    public DataManager $dataManager;
    public AthleteManager $athleteMgr;
    public ActivityManager $activityMgr;
    public function __construct()
    {
        $this->dataManager = new StubData();
        $authService = new \Stub\AuthService($this->dataManager->userRepository, new \Shared\HashPassword());
        $notificationService = new NotificationService($this->dataManager->notificationRepository,$this->dataManager->userRepository);
        $relationshipService = new RelationshipService($this->dataManager->relationshipRequestRepository,$notificationService);
        $this->userMgr = new UserManager($this->dataManager,$authService,$relationshipService);
        $this->athleteMgr = new AthleteManager($this->dataManager,$authService);
        $this->coachMgr = new CoachManager($this->dataManager,$authService);
        $this->activityMgr = new ActivityManager($this->dataManager,$authService);
    }
}