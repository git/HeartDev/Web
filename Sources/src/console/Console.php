<?php

namespace Console;

use DateTime;
use Model\Athlete;
use Model\Coach;
use Model\RelationshipRequest;
use Model\Role;
use Model\Training;
use Model\User;
use Manager\DataManager;
use Stub\StubData;


$model = new Model(); // Couche d'accès au model
function clearScreen()
{
    system('clear || cls');
}

function displayAuthMenu()
{
    clearScreen();
    echo "\n\n";
    echo "  +--------------------------+\n";
    echo "  |     Authentification     |\n";
    echo "  +--------------------------+\n";
    echo "  | 1. Se connecter          |\n";
    echo "  | 2. S'inscrire            |\n";
    echo "  | 0. Quitter               |\n";
    echo "  +--------------------------+\n";
    echo "  Choisissez une option: ";
}

function displayMainMenu()
{
    clearScreen();
    echo "\n--- Menu Principal ---\n";
    echo "1. Accueil\n";
    echo "2. Profil\n";
    echo "3. Analyse de la fréquence cardiaque\n";
    echo "4. Gestion sociale\n";
    echo "5. Athlètes (Coach seulement)\n";
    echo "6. Paramètres\n";
    echo "7. Notifications\n";
    echo "0. Se déconnecter\n";
    echo "Choisissez une option: ";
}

function displayProfileMenu()
{
    clearScreen();
    echo "\n--- Profil ---\n";
    echo "1. Informations de l'utilisateur\n";
    echo "2. Historique d'activité\n";
    echo "3. Liste d'amis\n";
    echo "4. Paramètres de confidentialité et visibilité\n";
    echo "5. Ajouter une activité\n";
    // Importer des données (FIT/GPX/TCX)/Manuel
    // Synchroniser l'appareil de fréquence cardiaque
    // Synchroniser l'app mobile
    echo "0. Retour au menu principal\n";
    echo "Choisissez une option: ";
}

function displayHeartRateAnalysisMenu()
{
    clearScreen();
    echo "\n--- Analyse de la fréquence cardiaque ---\n";
    echo "1. Options d'importation des données\n";
    echo "2. Graphiques et analyses\n";
    echo "3. Outils de partage\n";
    echo "0. Retour au menu principal\n";
    echo "Choisissez une option: ";
}

function displayCoachMenu()
{
    clearScreen();
    echo "\n--- Menu Coach ---\n";
    echo "1. Liste des athlètes\n";
    echo "3. Analyses par athlète\n";
    echo "4. Gérer la liste de mes athlètes\n";
    // Gérer les athlètes (comprend : Ajouter un athlète, Supprimer un athlète, Consulter les statistiques d'un athlète)
    echo "5. Gérer la liste de mes exercices\n";
    echo "0. Retour au menu principal\n";
    echo "Choisissez une option: ";
}

function displaySocialManagementMenu() {
    clearScreen();
    echo "\n--- Gestion sociale ---\n";
    echo "1. Rechercher des coach\n";
    echo "2. Rechercher des athletes\n";
    echo "3. Gérer la liste d'amis\n";
    echo "4. Options de partage\n";
    echo "0. Retour au menu principal\n";
    echo "Choisissez une option: ";
}
function displaySubSocialManagementMenu(){
    clearScreen();
    echo "\n--- Gestion sociale Sub 3---\n";
    echo "    3.1. Voir les demandes d'amitié\n";
    echo "    3.2. Répondre à une demande d'amitié\n";
    echo "    3.3. Ajouter un ami\n";
    echo "    3.4. Supprimer un ami\n";
}

function displaySettingsMenu()
{
    clearScreen();
    echo "\n--- Paramètres ---\n";
    echo "1. Gérer les informations de compte\n";
    // Modifier le profil du athlète et coach
    echo "2. Personnaliser le profil public\n";
    echo "3. Configurer les alertes\n";
    echo "4. Supprimer mon compte";
    echo "0. Retour au menu principal\n";
    echo "Choisissez une option: ";
}

function displayManagementArrayAthlete()
{
    clearScreen();
    echo "\n--- Menu Management Groupe Athlete ---\n";
    echo "1. Ajouter un athlète\n";
    echo "2. Supprimer un athlète\n";
    echo "3. Voir tout les athlètes de la liste\n";
    echo "0. Retour au menu du coach\n";
    echo "Choisissez une option: ";
}

function displayManagementArrayTraining()
{
    clearScreen();
    echo "\n--- Menu Management des entrainements ---\n";
    echo "1. Ajouter un entrainement\n";
    echo "2. Supprimer un entrainement\n";
    echo "3. Voir tout les entrainements de la liste\n";
    echo "0. Retour au menu du coach\n";
    echo "Choisissez une option: ";
}

/**
 * Fonction permettant à un utilisateur de se connecter.
 *
 * @param Model $model La couche d'accès au modèle de données.
 * @return bool Retourne true si l'utilisateur s'est connecté avec succès, sinon false.
 */
function loginUser(Model $model): bool
{
    try {
        echo "\nEntrez votre nom email: ";
        $emailUser = trim(fgets(STDIN));
        echo "Entrez votre mot de passe: ";
        $password = trim(fgets(STDIN));

        if ($model->userMgr->login($emailUser, $password)) {
            return true;
        } else {
            echo "Erreur de connexion. Essayez encore.\n";
            sleep(2);
            return false;
        }
    } catch (\Exception $e) {
        // Handle other exceptions
        echo "Une erreur s'est produite : " . $e->getMessage() . "\n";
        sleep(2);
        return false;
    }
}

function addFriend(Model $model) {
    clearScreen();
    echo "\nEntrez le nom d'utilisateur de la personne que vous recherchez : ";
    $nom = trim(fgets(STDIN));

    $userList = $model->userMgr->searchUsersByName($nom);
    if (empty($userList)) {
        echo "Aucun utilisateur trouvé.\n";
        return;
    }

    /** @var User $user */
    foreach ($userList as $index => $user) {
        echo ($index + 1) . ". " . $user->getNom() . " " . $user->getPrenom() . "\n";
    }


    echo "\nEntrez le numéro de la personne que vous voulez ajouter ou 0 pour annuler : ";
    $choice = trim(fgets(STDIN));

    if ($choice === '0') {
        echo "Ajout d'ami annulé.\n";
        return;
    }

    $selectedIndex = intval($choice) - 1;
    if (!isset($userList[$selectedIndex])) {
        echo "Sélection invalide.\n";
        return;
    }

    $user = $userList[$selectedIndex];
    echo "Ajout de " . $user->getPrenom() . " " . $user->getNom() . "\n";

    if ($model->userMgr->addFriend($user->getUsername())) {
        echo $userList[0]->getRole()->getUsersRequests()[0];
        echo $userList[0]->getNotifications()[0]->getType();

        sleep(2);

        echo "Notification envoyée.\n";
    } else {
        echo "Problème à l'envoi de la notification, veuillez vérifier votre connexion ou réessayer plus tard.\n";
    }
}


function deleteFriend(Model $model) {
    echo "\nListe de vos amis :\n";

    $friendList = $model->userMgr->getCurrentUser()->getRole()->getUsersList();

    if (empty($friendList)) {
        echo "Vous n'avez aucun ami à supprimer.\n";
        return;
    }

    /** @var User $friend */
    foreach ($friendList as $index => $friend) {
        echo ($index + 1) . ". " . $friend->getNom() . " " . $friend->getPrenom() . "\n";
    }

    echo "\nEntrez le numéro de l'ami que vous voulez supprimer ou 0 pour annuler : ";
    $choice = trim(fgets(STDIN));

    if ($choice === '0') {
        echo "Suppression d'ami annulée.\n";
        return;
    }

    $selectedIndex = intval($choice) - 1;
    if (!isset($friendList[$selectedIndex])) {
        echo "Sélection invalide.\n";
        return;
    }

    $friend = $friendList[$selectedIndex];
    if ($model->userMgr->deleteFriend($friend->getId())) { // Supposition que deleteFriend utilise l'ID de l'ami
        echo "Ami " . $friend->getPrenom() . " " . $friend->getNom() . " supprimé.\n";
    } else {
        echo "Problème lors de la suppression, veuillez réessayer plus tard.\n";
    }
}

function answerAdd(Model $model) {
    echo "\nListe des demandes d'ami en attente :\n";

    $friendRequests = $model->userMgr->getCurrentUser()->getRole()->getUsersRequests();

    if (empty($friendRequests)) {
        echo "Aucune demande d'ami en attente.\n";
        return;
    }

    foreach ($friendRequests as $index => $request) {
        echo ($index + 1) . ". Demande de : " . $request->getId() . "\n";
    }

    echo "\nEntrez le numéro de la demande à répondre ou 0 pour annuler : ";
    $choice = trim(fgets(STDIN));

    if ($choice === '0') {
        echo "Aucune action effectuée.\n";
        return;
    }

    $selectedIndex = intval($choice) - 1;
    if (!isset($friendRequests[$selectedIndex])) {
        echo "Sélection invalide.\n";
        return;
    }
    /** @var RelationshipRequest $request */
    $request = $friendRequests[$selectedIndex];
    echo "Répondre à la demande de " . $request->getFromUser() . " " . $request->getToUser() . ". Accepter (oui) ou refuser (non) ? ";
    $response = strtolower(trim(fgets(STDIN)));

    if ($response === 'oui') {
        if ($model->userMgr->respondToFriendRequest($request->getId(),true)) { // Supposition de la méthode acceptFriendRequest
            echo $model->userMgr->getCurrentUser()->getRole()->getUsersList()[0];
            echo "Demande d'ami acceptée.\n";
        } else {
            echo "Problème lors de l'acceptation de la demande, veuillez réessayer plus tard.\n";
        }
    } elseif ($response === 'non') {
        if ($model->userMgr->respondToFriendRequest($request->getId(),false)) { // Supposition de la méthode rejectFriendRequest
            echo "Demande d'ami refusée.\n";
        } else {
            echo "Problème lors du refus de la demande, veuillez réessayer plus tard.\n";
        }
    } else {
        echo "Réponse non reconnue. Aucune action effectuée.\n";
    }
}


/**
 * Fonction permettant à un utilisateur de s'inscrire.
 *
 * @param Model $model La couche d'accès au modèle de données.
 * @return bool Retourne true si l'inscription a réussi, sinon false.
 */
function registerUser(Model $model)
{
    try {

        echo "\nEntrez votre nom: ";
        $nom = trim(fgets(STDIN));

        echo "Entrez votre prénom: ";
        $prenom = trim(fgets(STDIN));

        echo "Entrez votre username: ";
        $username = trim(fgets(STDIN));

        echo "Entrez votre adresse email: ";
        $email = trim(fgets(STDIN));

        echo "Entrez votre mot de passe: ";
        $motDePasse = trim(fgets(STDIN));

        echo "Entrez votre sexe ( M :Homme / F :Femme ): ";
        $sexe = trim(fgets(STDIN));

        echo "Entrez votre taille (en mètres): ";
        $taille = floatval(trim(fgets(STDIN)));

        echo "Entrez votre poids (en kilogrammes): ";
        $poids = floatval(trim(fgets(STDIN)));

        echo "Entrez votre date de naissance (au format YYYY-MM-DD): ";
        $dateNaissanceStr = trim(fgets(STDIN));
        $dateNaissance = new \DateTime($dateNaissanceStr);


        if (!$dateNaissance) {
            throw new \Exception("Date de naissance non valide. Format attendu : YYYY-MM-DD");
        }

        echo "Entrez votre rôle (ex. Athlete,Coach , etc.): ";
        $roleName = trim(fgets(STDIN));

        $registrationData = [
            'nom' => $nom,
            'prenom' => $prenom,
            'username' => $username,
            'email' => $email,
            'sexe' => $sexe,
            'taille' => $taille,
            'poids' => $poids,
            'dateNaissance' => $dateNaissance,
            'roleName' => $roleName
        ];
        if ($model->userMgr->register($email, $motDePasse,$registrationData)) {
            echo "Inscription réussie. Connexion automatique...\n";
            return true;
        } else {
            echo "L'inscription a échoué. Veuillez réessayer.\n";
            return false;
        }

    } catch (\Exception $e) {

        echo "Erreur lors de l'inscription : " . $e->getMessage() . "\n";
        sleep(2);
        return false;
    }
}

function ArrayAthleteMenu(Model $model)
{
    do {
        displayManagementArrayAthlete();
        $coachChoice = trim(fgets(STDIN));

        switch ($coachChoice) {
            case '1':
                echo "Renseignez le surnom de l'utilisateur : ";
                $username = trim(fgets(STDIN));

                if($model->coachMgr->addUser($username)){
                    echo "Ajout avec succès !";
                } else {
                    echo "Le user ne peut pas être ajouter en tant que athlete !";
                }
                sleep(2);
                break;
            case '2':
                echo "Renseignez le surnom de l'utilisateur : ";
                $username = trim(fgets(STDIN));
                if($model->coachMgr->removeUser($username)){
                    echo "Suppression avec succès !";
                } else {
                    echo "Pb suppression ou aucun utilisateur de ce nom !";
                }
                sleep(2);
                break;
            case '3':
                $usersArray = $model->coachMgr->getUsersList();
                if (!empty($usersArray)) {
                    foreach ($usersArray as $value) {
                        echo $value->__toString() . "\n";
                    }
                } else {
                    echo "Aucun utilisateur dans la liste\n";
                }
                sleep(2);
                break;
            case '0':
                return;
            default :
                echo "Option invalide. Veuillez réessayer.\n";
                sleep(2);
                break;
        }
    } while($coachChoice);
}

function ArrayTrainingMenu(Model $model)
{
    do {
        displayManagementArrayTraining();
        $coachChoice = trim(fgets(STDIN));

        switch ($coachChoice) {
            case '1':
                $existingTrainings = $model->coachMgr->getTrainingsList();
                $lastTraining = end($existingTrainings);
                $lastTrainingId = $lastTraining ? $lastTraining->getId() : 0;
                $newTrainingId = $lastTrainingId + 1;

                echo "L'ID de l'entraînement sera automatiquement défini sur : $newTrainingId\n";
                $date = new DateTime();
                echo "Renseignez la latitude de l'entraînement : ";
                $latitude = trim(fgets(STDIN));
                echo "Renseignez la longitude de l'entraînement : ";
                $longitude = trim(fgets(STDIN));
                echo "Renseignez la description de l'entraînement : ";
                $description = trim(fgets(STDIN));

                $training = new Training($newTrainingId, $date, $latitude, $longitude, $description, null);

                if($model->coachMgr->addTraining($training)){
                    echo "Ajout avec succès !";
                } else {
                    echo "Pb ajout !";
                }
                sleep(2);
                break;

            case '2':
                echo "Renseignez l'id de l'entrainement : ";
                $idTraining = trim(fgets(STDIN));
                if($model->coachMgr->removeTraining($idTraining)){
                    echo "Suppression avec succès !";
                } else {
                    echo "Pb suppression ou aucun entrainement de cet id !";
                }
                sleep(2);
                break;
            case '3':
                $trainingArray = $model->coachMgr->getTrainingsList();
                if (!empty($trainingArray)) {
                    foreach ($trainingArray as $value) {
                        echo $value->__toString() . "\n";
                    }
                } else {
                    echo "Aucun entrainement dans la liste\n";
                }
                sleep(2);
                break;
            case '0':
                return;
            default :
                echo "Option invalide. Veuillez réessayer.\n";
                sleep(2);
                break;
        }
    } while($coachChoice);
}
function ArrayFriendManagementMenu(Model $model) {
    do {
        displaySubSocialManagementMenu();
        $userChoice = trim(fgets(STDIN));

        switch ($userChoice) {
            case '3.1': // Voir les demandes d'amitié
                $friendRequests = $model->userMgr->getCurrentUser()->getRole()->getUsersRequests();
                if (!empty($friendRequests)) {
                    foreach ($friendRequests as $request) {
                        echo $request->__toString() . "\n";
                    }
                } else {
                    echo "Aucune demande d'amitié.\n";
                }
                sleep(2);
                break;

            case '3.2': // Répondre une demande d'amitié
                answerAdd($model);
                break;

            case '3.3': // Ajouter un ami
                addFriend($model);
                sleep(2);
                break;

            case '3.4': // Supprimer un ami
                deleteFriend($model);
                sleep(2);
                break;

            case '0': // Retour au menu principal
                return;

            default:
                echo "Option invalide. Veuillez réessayer.\n";
                sleep(2);
                break;
        }
    } while ($userChoice);
}

//function displayCoachMenu()
//{
//    clearScreen();
//    echo "\n--- Menu Coach ---\n";
//    echo "1. Liste des athlètes\n";
//    echo "2. Statistiques globales\n";
//    echo "3. Analyses par athlète\n";
//    echo "4. Gérer la liste de mes athlètes\n";
//    // Gérer les athlètes (comprend : Ajouter un athlète, Supprimer un athlète, Consulter les statistiques d'un athlète)
//    echo "5. Gérer la liste de mes exercices\n";
//    echo "0. Retour au menu principal\n";
//    echo "Choisissez une option: ";
//}
function CoachMenu(Model $model)
{
    do {
        displayCoachMenu();
        $coachChoice = trim(fgets(STDIN));

        switch ($coachChoice) {
            case '1': // echo "1. Liste des athlètes\n";
                $arrayUsers = $model->coachMgr->getUsersList();
                if (isset($arrayUsers) && !empty($arrayUsers)) {
                    foreach ($arrayUsers as $value) {
                        echo $value->__toString() . "\n";
                    }
                } else {
                    echo "Aucun utilisateur dans la liste\n";
                }
                sleep(2);
                break;
//            case '2': //    echo "2. Statistiques globales\n";
//                $arrayUsers = $model->coachMgr->getUsersList();
//
//                if (!empty($arrayUsers)) {
//                    do {
//                        clearScreen();
//                        $cpt = 0;
//                        foreach ($arrayUsers as $value) {
//                            echo $cpt . " - " . $value->__toString() . "\n";
//                            $cpt = $cpt + 1;
//                        }
//
//                        echo "Renseignez le numéro de l'utilisateur choisi : ";
//                        $usernameNumber = trim(fgets(STDIN));
//
//                        // Vérifier si l'index saisi est valide
//                        if (isset($arrayUsers[$usernameNumber])) {
//                            $selectedUser = $arrayUsers[$usernameNumber];
//                            if (($arrayStats = $model->coachMgr->getStatistics($selectedUser))) {
//                                foreach ($arrayStats as $value) {
//                                    echo $value->__toString() . "\n";
//                                }
//                            } else {
//                                echo "Pas de statistiques valides présentent !\n";
//                            }
//                        } else {
//                            echo "Numéro d'utilisateur non valide.\n";
//                            $cpt = 0;
//                        }
//                    } while($cpt == 0);
//                } else {
//                    echo "Aucun utilisateur dans la liste.\n";
//                }
//                sleep(2);
//                break;
            case '3': //    echo "3. Analyses par athlète\n";
                $arrayUsers = $model->coachMgr->getUsersList();

                if (!empty($arrayUsers)) {
                    do {
                        clearScreen();
                        $cpt = 0;
                        foreach ($arrayUsers as $value) {
                            echo $cpt . " - " . $value->__toString() . "\n";
                            $cpt = $cpt + 1;
                        }

                        echo "Renseignez le numéro de l'utilisateur choisi : ";
                        $usernameNumber = trim(fgets(STDIN));

                        // Vérifier si l'index saisi est valide
                        if (isset($arrayUsers[$usernameNumber])) {
                            $selectedUser = $arrayUsers[$usernameNumber];
                            if (($arrayStats = $model->coachMgr->getAnalyse($selectedUser))) {
                                foreach ($arrayStats as $value) {
                                    echo $value->__toString() . "\n";
                                }
                            } else {
                                echo "Pas d'Analyses valides présentent !\n";
                            }
                        } else {
                            echo "Numéro d'utilisateur non valide.\n";
                            $cpt = 0;
                        }
                    } while($cpt == 0);
                } else {
                    echo "Aucun utilisateur dans la liste.\n";
                }
                sleep(2);
                break;
            case '4': //    echo "4. Gérer la liste de mes athlètes\n";
                ArrayAthleteMenu($model);
                break;
            case '5': //    echo "5. Gérer la liste de mes exercices\n";
                ArrayTrainingMenu($model);
                break;
            case '0': // Quitter
                return;
            default:
                echo "Option invalide. Veuillez réessayer.\n";
                sleep(2);
                break;
        }
    } while($coachChoice);
}

//function displaySocialManagementMenu()
//{
//    clearScreen();
//    echo "\n--- Gestion sociale ---\n";
//    echo "1. Rechercher des coach\n";
//    echo "2. Rechercher des athletes\n";
//    echo "3. Gérer la liste d'amis\n";
//    // Ajouter des amis
//    // Supprimer des amis ...
//    echo "4. Options de partage\n";
//    echo "0. Retour au menu principal\n";
//    echo "Choisissez une option: ";
//}
// TODO athlteMgr
function socialManagementMenu(Model $model) {
    do {
        displaySocialManagementMenu();
        $managementChoice = trim(fgets(STDIN));

        switch ($managementChoice) {
            case '1':
                echo "Renseignez le surnom du coach que vous recherchez : ";
                $coachUsername = trim(fgets(STDIN));
                $users = $model->userMgr->searchUsersByName($coachUsername);

                if (!empty($users)) {
                    $foundCoaches = false;
                    foreach ($users as $user) {
                        if ($user->getRole() instanceof Coach) {
                            echo $user->__toString();
                            $foundCoaches = true;
                        }
                    }
                    if (!$foundCoaches) {
                        echo "Aucun coach de ce nom : $coachUsername\n";
                    }
                } else {
                    echo "Aucun utilisateur trouvé avec le surnom : $coachUsername\n";
                }
                sleep(2);
                break;
            case '2':
                echo "Renseignez le surnom de l'athlete que vous recherchez : ";
                $athleteUsername = trim(fgets(STDIN));
                $users = $model->userMgr->searchUsersByName($athleteUsername);

                if (!empty($users)) {
                    $foundAthletes = false;
                    foreach ($users as $user) {
                        if ($user->getRole() instanceof Athlete) {
                            echo $user->__toString();
                            $foundAthletes = true;
                        }
                    }
                    if (!$foundAthletes) {
                        echo "Aucun athlete de ce nom : $athleteUsername\n";
                    }
                } else {
                    echo "Aucun utilisateur trouvé avec le surnom : $athleteUsername\n";
                }
                sleep(2);
                break;
            case '3': // 3. Gérer la liste d'amis
                ArrayFriendManagementMenu($model);
                return;
            case '0': //    echo "0. Retour au menu principal\n";
                return;
            default:
                echo "Option invalide. Veuillez réessayer.\n";
                sleep(2);
                break;
        }
    } while($managementChoice);
}

function profileMenu(Model $model)
{
    do {
        displayProfileMenu();
        $athleteChoice = trim(fgets(STDIN));

        switch ($athleteChoice) {
            case '1':
                echo $model->userMgr->getCurrentUser();
                sleep(2);
                break;
            case '2':
                if($model->userMgr->getCurrentUser()->getRole() instanceof Athlete) {
                    $activities = $model->athleteMgr->getActivities();
                    if($activities !== null && count($activities) > 0) {
                        foreach ($activities as $activity) {
                            echo $activity->__toString();
                        }
                    } else {
                        echo "No activities found";
                    }
                } else {
                    echo "Vous etes pas un athléte";
                }
                sleep(2);
                break;
            case '3': // Liste d'amis
                sleep(2);
                break;
            case '4': // Importer des données (FIT/GPX/TCX)
                echo "Veuillez renseigner le chemin du fichier :\n";
                $passFile = trim(fgets(STDIN));
                echo "Veuillez renseigner le type d'activité :\n";
                $typeActivity = trim(fgets(STDIN));
                echo "Veuillez renseigner l'effort resenti (de 0 à 5) :\n";
                do {
                    $effort = trim(fgets(STDIN));
                } while ($effort < 0 || $effort > 5);
                $isAddActivity = $model->activityMgr->uploadFile($typeActivity, $effort, $passFile);
                echo $isAddActivity ? "Activité ajoutée avec succès" : "Erreur lors de l'ajout de l'activité";
            case '0':
                return;
            default :
                echo "Option invalide. Veuillez réessayer.\n";
                sleep(2);
                break;
        }
    } while($athleteChoice);
}

while (true) {
    $loggedIn = false;

    if (!$loggedIn) {
        displayAuthMenu();
        $choice = trim(fgets(STDIN));

        switch ($choice) {

            case '1': // Se connecter
                if($model->userMgr->login("john.doe@example.com", "password123"))
                    $loggedIn = true;

                /* if (loginUser($model)) {
                     $loggedIn = true;
                 }*/
                break;
            case '2': // S'inscrire
                if($model->userMgr->login("bruce.lee@example.com", "hello321"))
                    $loggedIn = true;
//                if (registerUser($model)) {
//                    $loggedIn = true;
//                }
                break;

            case '0': // Quitter
                echo "Merci d'avoir utilisé notre application. Au revoir !\n";
                exit(0);

            default:
                echo "Option invalide. Veuillez réessayer.\n";
                break;
        }
    }
    if ($loggedIn){
        while ($loggedIn) {
            displayMainMenu();
            $mainChoice = trim(fgets(STDIN));

            switch ($mainChoice) {
                case '1': // Accueil
                    echo "Affichage de l'accueil...\n";
                    break;

                case '2': // Profil
                    profileMenu( $model);
                    break;

                case '3': // Analyse de la fréquence cardiaque
                    displayHeartRateAnalysisMenu();
                    $analysisChoice = trim(fgets(STDIN));
                    // TODO WEB
                    break;

                case '4': // Gestion sociale
                    socialManagementMenu($model);
                    // TODO: Ajouter la logique pour les options de gestion sociale ici.
                    break;

                case '5': // Coach
                    if($model->userMgr->getCurrentUser()->getRole() instanceof \Model\Coach) {
                        CoachMenu($model);
                    } else {
                        echo "Vous n'avez pas accès à cette section ! (il faut etre coach)\n";
                        sleep(2);
                    }
                    break;

                case '6': // Paramètres
                    displaySettingsMenu();
                    $settingsChoice = trim(fgets(STDIN));
                    // TODO: Ajouter la logique pour les options de paramètres ici.
                    break;

                case '7': // Notifications
                    
                    echo "Affichage des notifications...\n";
                    break;

                case '0': // Se déconnecter
                    $loggedIn = false;
                    break; // Sortir de la boucle interne pour revenir à l'écran d'authentification.

                default:
                    echo "Option invalide. Veuillez réessayer.\n";
                    break;
            }
        }
    }
}

?>