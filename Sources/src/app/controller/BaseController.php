<?php

namespace App\Controller;
use Data\Core\Preferences;

use App\Container;
use App\Router\Response\RedirectResponse;
use App\Router\Response\Response;
use Psr\Container\ContainerInterface;


abstract class BaseController
{

    protected Preferences $preference;

    public function __construct(){
        $this->preference = new Preferences();
    }
    protected ContainerInterface $container;

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function renderView(string $view, array $parameters = []): string
    {
        if (!$this->container->has(\Twig\Environment::class)) {
            throw new \LogicException('You cannot use the "renderView" method if the Twig Bundle is not available. Try running "composer require symfony/twig-bundle".');
        }

        return $this->container->get(\Twig\Environment::class)->render($view, $parameters);
    }
    /**
     * Renders a view.
     *
     * If an invalid form is found in the list of parameters, a 422 status code is returned.
     * Forms found in parameters are auto-cast to form views.
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $content = $this->renderView($view, $parameters);
        $response ??= new Response();

       /* if (200 === $response->getStatusCode()) {
            foreach ($parameters as $v) {
                if ($v instanceof FormInterface && $v->isSubmitted() && !$v->isValid()) {
                    $response->setStatusCode(422);
                    break;
                }
            }
        }*/

        $response->setContent($content);

        return $response;
    }

    protected function redirect(string $url, int $status = 302): RedirectResponse
    {
        return new RedirectResponse($url, $status);
    }

    protected function redirectToRoute(string $route, array $parameters = [], int $status = 302): RedirectResponse
    {
        return $this->redirect($this->generateUrl($route, $parameters), $status);
    }

    /*
     * TODO : Should hanle ierror if the route is not existing
     * */
    protected function generateUrl(string $route, array $parameters = []): string
    {
        return $this->container->get(\App\Router\Router::class)->generate($route, $parameters);
    }


}