<?php

namespace App\Controller;

use App\Container;
use App\Router\Request\IRequest;
use App\Router\Response\Response;
use App\Router\Response\IResponse;

use Couchbase\UserManager;
use Shared\Attributes\Route;
use Shared\Validation;
use Twig\Environment;
use Data\Core\Preferences;
use Shared\Log;

class AuthController extends BaseController
{


    #[Route('/login', name: 'login',methods: ['POST'])]
    public function login(IRequest $request): IResponse {
        
        $error = [];
        try {
            $log=Validation::clean_string($request->getBody()['email']);
            $mdp=Validation::clean_string($request->getBody()['password']);
        } catch (\Throwable $th) {
            $error = "Wrong cred";
        }
        
        if($this->container->get(UserManager::class)->login($log,$mdp)){
            return $this->redirectToRoute('/');
        }
        else{
            $error [] = "Erreur de connexion. Essayez encore"; 
        }
        return $this->render('./page/login.html.twig', ['error' => $error]);
        
        
        
    }

    #[Route('/log', name: 'baseLog',methods: ['GET'])]
    public function index(IRequest $request): IResponse { 

        return $this->render('./page/login.html.twig',[
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);

    }

    #[Route('/register', name: 'register' , methods:['GET'])]
    public function register(IRequest $request): IResponse
    {
        if ($request->getMethod() == 'POST') {
            $nom = $request->getBody()['nom'];
    
            $prenom = $request->getBody()['prenom'];
    
            $username = $request->getBody()['username'];
    
            $email = $request->getBody()['email'];
    
            $motDePasse = $request->getBody()['motDePasse'];
    
            $sexe = $request->getBody()['sexe'];
    
            $taille = $request->getBody()['taille'];
    
            $poids = $request->getBody()['poids'];
    
            $dateNaissanceStr = $request->getBody()['nom'];
            $dateNaissance = new \DateTime($dateNaissanceStr);
    
    
            if (!$dateNaissance) {
                throw new \Exception("Date de naissance non valide. Format attendu : YYYY-MM-DD");
            }
    
            $roleName = $request->getBody()['roleName'];


            $registrationData = [
                'nom' => $nom,
                'prenom' => $prenom,
                'username' => $username,
                'email' => $email,
                'sexe' => $sexe,
                'taille' => $taille,
                'poids' => $poids,
                'dateNaissance' => $dateNaissance,
                'roleName' => $roleName
            ];

            try {
                if ($this->container->get(UserManager::class)->register($email, $motDePasse, $registrationData)) {
                    return $this->redirectToRoute('/');
                } else {
            
                    $error [] = 'L\'inscription a échoué. Veuillez réessayer.';
                }
            } catch (\Exception $e) {
                $error [] = 'Erreur lors de l\'inscription: ' . $e->getMessage(); 
            }
        }

        return $this->render('/register.html.twig');
    }

    #[Route(path: '/mdp', name: 'mdp', methods: ['POST'])]
    public function mdp(string $ancienMotDePasse,string $nouveauMotDePasse,string $confirmerMotDePasse, IRequest $req): Response
    {
        
       // CONFIRMER LES DONNESS !!!!! IMPORTANT

        return $this->render('./page/settings.html.twig',[
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);
    }

    
   

    
}
?>