<?php

namespace App\Controller;

use App\Container;
use App\Router\Request\IRequest;
use App\Router\Response\Response;
use Shared\Attributes\Route;
use Twig\Environment;
use Data\Core\Preferences;
use Shared\Log;

class UserController extends BaseController
{


    #[Route(path: '/', name: 'home', methods: ['GET'])]
    public function index(): Response
    {
        
        return $this->render('./page/home.html.twig',[
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);
    }


    #[Route(path: '/settings', name: 'settings', methods: ['GET'])]
    public function settings(IRequest $req): Response
    {
        return $this->render('./page/settings.html.twig',[
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);
    }


    #[Route(path: '/preferences', name: 'preferences', methods: ['POST'])]
    public function preferences(string $theme, IRequest $req): Response
    {

        //  VALIDER LES DONNEES
        $this->preference->majCookie($theme);
       
        return $this->render('./page/settings.html.twig',[
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);
    }
    
}