<?php

namespace App\Controller;

use App\Container;
use App\Router\Request\IRequest;
use App\Router\Response\Response;
use Shared\Attributes\Route;
use Twig\Environment;
use Data\Core\Preferences;
use Shared\Log;

class AthleteController extends BaseController
{

    #[Route(path: '/search-ath', name: 'search-ath', methods: ['GET'])]
    public function searchUser(string $username, IRequest $req): Response
    {
        $taberror = [];
        $utiliArray = [
            [
                'nom' => 'John',
                'prenom' => 'Doe',
                'img' => 'john_doe',
                'username' => 'johndoe',
            ],
            [
                'nom' => 'Alice',
                'prenom' => 'Smith',
                'img' => 'alice_smith',
                'username' => 'alicesmith',
            ],
        ];
        // if(!Validation::val_string($name)){
        try {
            //code...
            //     $model->userMgr->getUser($name);
            return $this->render('./page/addfriend.html.twig', [
                'css' => $this->preference->getCookie(),
                'pp' => "test2",
                'user' => "Doe",
                'role' => "Athlète",
                'friendship' => [],
                'analyzes' => [],
                'mails' => [],
                'users' => $utiliArray,
                'infoUser' => [],
                'exos' => [],
                'member' => [],
                'responce' => "Notification d'ajout envoyée à $username"
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            // return $this->render("addfriend.html.twig", ['tabError' => $taberror ]);
        }
        // }

    }

    #[Route(path: '/analyses', name: 'analyses', methods: ['GET'])]
    public function analyses(): Response
    {
        return $this->render('./page/analyze.html.twig', [
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);
    }


    #[Route(path: '/exercice', name: 'exercice', methods: ['GET'])] // 8
    public function exercice(): Response
    {
        return $this->render('./page/exercice.html.twig', [
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => [],
            'infoUser' => [],
            'exos' => [],
            'member' => []
        ]);
    }

    #[Route(path: '/add-friend', name: 'add-friend', methods: ['POST'])]
    public function addFriend(string $username, IRequest $req): Response
    {
        $taberror = [];
        $utiliArray = [
            [
                'nom' => 'John',
                'prenom' => 'Doe',
                'img' => 'john_doe',
                'username' => 'johndoe',
            ],
            [
                'nom' => 'Alice',
                'prenom' => 'Smith',
                'img' => 'alice_smith',
                'username' => 'alicesmith',
            ],
        ];
        // if(!Validation::val_string($name)){
        try {
            //code...
            //     $model->userMgr->addFriend($name);
            return $this->render('./page/addfriend.html.twig', [
                'css' => $this->preference->getCookie(),
                'pp' => "test2",
                'user' => "Doe",
                'role' => "Athlète",
                'friendship' => [],
                'analyzes' => [],
                'mails' => [],
                'users' => $utiliArray,
                'infoUser' => [],
                'exos' => [],
                'member' => [],
                'responce' => "Notification d'ajout envoyée à $username"
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            // return $this->render("addfriend.html.twig", ['tabError' => $taberror ]);
        }
        // }

    }



    
    #[Route(path: '/friend', name: 'friend', methods: ['GET'])]
    public function friend(): Response
    {
        $utiliArray = [
            [
                'nom' => 'John',
                'prenom' => 'Doe',
                'img' => 'john_doe',
                'username' => 'johndoe',
            ],
            [
                'nom' => 'Alice',
                'prenom' => 'Smith',
                'img' => 'alice_smith',
                'username' => 'alicesmith',
            ],
        ];
        return $this->render('./page/addfriend.html.twig',[
            'css' => $this->preference->getCookie(),
            'pp' => "test2",
            'user' => "Doe",
            'role' => "Athlète",
            'friendship' => [],
            'analyzes' => [],
            'mails' => [],
            'users' => $utiliArray,
            'infoUser' => [],
            'exos' => [],
            'member' => [],
        ]);
    }

}