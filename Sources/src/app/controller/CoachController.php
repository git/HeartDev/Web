<?php

// namespace App\Controller;

// use App\Container;
// use App\Router\Request\IRequest;
// use App\Router\Response\Response;
// use App\Router\Response\IResponse;

// use Shared\Attributes\Route;
// use Twig\Environment;
// use Data\Core\Preferences;
// use Shared\Log;



// #[Route(path: '/coach', name: 'coach')]
// class CoachController extends BaseController
// {

//     private ICoachManager $coachManager;
//     private $security;

//     public function __construct(DataManager $dataManager, Security $security)
//     {
//         $this->coachManager = $dataManager->coachMgr;
//         $this->security = $security;
//     }

//     #[Route(path: '/', name: 'home', methods: ['GET'])]
//     public function index(): Response
//     {
//         return $this->render('./page/home.html.twig',[
//             'css' => $this->preference->getCookie(),
//             'pp' => "test2",
//             'user' => "Doe",
//             'role' => "Athlète",
//             'friendship' => [],
//             'analyzes' => [],
//             'mails' => [],
//             'users' => [],
//             'infoUser' => [],
//             'exos' => [],
//             'member' => []
//         ]);
//     }
//     #[Route('/global-stats', name: 'coach_global_stats', methods: ['GET'])]
//     public function globalStats(): Response
//     {
//         // Add logic to fetch and process global stats
//         return $this->render('coach/global_stats.html.twig');
//     }

// #[Route(path: '/exercice', name: 'exercice', methods: ['GET'])] // 8
//     public function exercice(): Response
//     {
//         return $this->render('./page/exercice.html.twig',[
//             'css' => $this->preference->getCookie(),
//             'pp' => "test2",
//             'user' => "Doe",
//             'role' => "Athlète",
//             'friendship' => [],
//             'analyzes' => [],
//             'mails' => [],
//             'users' => [],
//             'infoUser' => [],
//             'exos' => [],
//             'member' => []
//         ]);
//     }

// #[Route(path: '/coaching', name: 'coaching', methods: ['GET'])]
// public function coaching(): Response
// {
//     return $this->render('./page/coaching.html.twig',[
//         'css' => $this->preference->getCookie(),
//         'pp' => "test2",
//         'user' => "Doe",
//         'role' => "Athlète",
//         'friendship' => [],
//         'analyzes' => [],
//         'mails' => [],
//         'users' => [],
//         'infoUser' => [],
//         'exos' => [],
//         'member' => []
//     ]);
// }

//     #[Route('/list-athletes', name: 'coach_list_athletes')]
//     public function listAthletes(): Response
//     {
//         $coach = $this->security->getUser();
//         $athletes = $this->coachManager->getAthletesForCoach($coach);
//         return $this->render('coach/list_athletes.html.twig', [
//             'athletes' => $athletes,
//         ]);
//     }
//     #[Route('/athlete-analysis/{athleteId}', name: 'coach_athlete_analysis', methods: ['GET'])]
//     public function athleteAnalysis($athleteId): Response
//     {
//         // Fetch and process data specific to the athlete
//         return $this->render('coach/athlete_analysis.html.twig', [
//             'athleteId' => $athleteId,
//         ]);
//     }
//     #[Route('/add-athlete/{athleteId}', name: 'coach_add_athlete', methods: ['POST'])]
//     public function addAthlete(IRequest $request, $athleteId): IResponse
//     {
//         // Implement logic to add athlete
//         // ...

//         return $this->redirectToRoute('coach_list_athletes');
//     }

//     // #[Route('/remove-athlete', name: 'coach_remove_athlete', methods: ['POST'])]
//     // public function removeAthlete(int $athleteId, IRequest $request): IResponse
//     // {
        
//     //     return $this->redirectToRoute("/athletes");
//     // }

    
// }
