<?php
    $listF= array (
        "a"  => array("img" => "test","nom" => "Kilou", "prenom" => "Clément", "status" => "En ligne"),
        "b" => array("img" => "test2","nom" => "Kilou", "prenom" => "Frédérique", "status" => "En ligne"),
        "c"   => array("img" => "test3","nom" => "Kilou", "prenom" => "Hugo", "status" => "Hors ligne")
    );

    $listMe= array (
        "a"  => array("img" => "test","nom" => "Kilou", "prenom" => "Clément", "status" => "En ligne"),
        "b" => array("img" => "test2","nom" => "Kilou", "prenom" => "Frédérique", "status" => "En ligne"),
        "c"   => array("img" => "test3","nom" => "Kilou", "prenom" => "Hugo", "status" => "Hors ligne")
    );

    $listA= array (
        "a"  => array("date" => "29/11/18","type" => "Nage", "bpm" => "116", "kmh" => "3.0","distance" => "0.6"),
        "b" => array("date" => "27/11/18","type" => "Sprint", "bpm" => "143", "kmh" => "19.0","distance" => "4.2"),
        "c"   => array("date" => "23/11/18","type" => "Cyclisme", "bpm" => "126", "kmh" => "27.0","distance" => "30.0")
    );



    $listM= array (
        "a"  => array("lu" => "false","nom" => "Julien", "prenom" => "Clément", "message" => "Demande d'exercice"),
        "b" => array("lu" => "true","nom" => "Ferdiloi", "prenom" => "Frédérique", "message" => "Coaching"),
        "c"   => array("lu" => "true","nom" => "Kuliqo", "prenom" => "Raphael", "message" => "Rendez-vous")
    );

    $listE= array (
        "a"  => array("date" => "20/10/23","type" => "Natation", "intensite" => "Fort", "status" => "Programmé"),
        "b" => array("date" => "16/10/23","type" => "Boxe anglaise", "intensite" => "Moyen", "status" => "Fini"),
        "c"   => array("date" => "10/10/23","type" => "Cyclisme", "intensite" => "Moyen", "status" => "Fini")
    );

    $infoUser= array("img" => "test2", "nom"=>"Polika", "prenom" => "Amandine", "dateNaiss" => "24/05/1993", "favSport" => "Natation", "coach" => "M.Bastien", "description" => "Salut, c'est Frigiel");
    
    $loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/views/Templates');
    $twig = new \Twig\Environment($loader);

    $template=$twig->load('./page/exercice.html.twig');
    $template->display([
        'pp' => "test2",
        'user' => "Amandine",
        'role' => "Athlète",
        'friendship' => $listF,
        'analyzes' => $listA,
        'mails' => $listM,
        'users' => $listF,
        'infoUser' => $infoUser,
        'exos' => $listE,
        'member' => $listMe
    ]);
?>