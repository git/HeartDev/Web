<?php
namespace App\Router;

/**
 * Represents a single route in the application.
 */
class Route
{
    /**
     * The name of the route.
     *
     * @var string|null
     */
    private ?string $name;

    /**
     * The path for the route.
     *
     * @var string
     */
    private string $path;

    /**
     * The callable to be executed when the route is matched.
     *
     * @var callable
     */
    private $callable;

    /**
     * Constructor for the Route.
     *
     * @param string $path The path for the route.
     * @param callable $callable The callable to be executed for this route.
     * @param array|null $params Optional parameters for the route.
     * @param string|null $name Optional name for the route.
     */
    public function __construct(string $path, callable $callable, array $params = null, string $name = null)
    {
        $this->path = $path;
        $this->callable = $callable;
        $this->name = $name;
    }

    /**
     * Gets the name of the route.
     *
     * @return string|null The name of the route.
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Sets the name of the route.
     *
     * @param string|null $name The name to set.
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets the callable associated with the route.
     *
     * @return callable The callable for this route.
     */
    public function getCallable()
    {
        return $this->callable;
    }

    /**
     * Gets the path for the route.
     *
     * @return string The path for the route.
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Sets the callable for the route.
     *
     * @param callable $callable The callable to set for this route.
     */
    public function setCallable(callable $callable)
    {
        $this->callable = $callable;
    }
}
