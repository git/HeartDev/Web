<?php

namespace App\Router\Response;

class RedirectResponse implements IResponse
{
    private $content;
    private $statusCode;
    private $headers;
    private $url;

    public function __construct(string $url, int $statusCode = 302, array $headers = [])
    {
        $this->url = $url;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
        $this->content = '';
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setHeader(string $key, string $value): void
    {
        $this->headers[$key] = $value;
    }

    public function send(): void
    {
        http_response_code($this->statusCode);

        foreach ($this->headers as $name => $value) {
            header("$name: $value");
        }
        
        header("Location: " . $this->url);

        // Optionally echo content if any
        echo $this->content;

        exit();
    }
}
