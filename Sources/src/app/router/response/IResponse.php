<?php
namespace App\Router\Response;

interface IResponse {
    public function getContent(): string;
    public function setContent(string $content): void;
    public function getStatusCode(): int;
    public function setStatusCode(int $statusCode): void;
    public function getHeaders(): array;
    public function setHeader(string $key, string $value): void;
    public function send(): void;
}