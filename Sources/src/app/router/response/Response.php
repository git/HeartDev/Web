<?php
namespace App\Router\Response;

class Response implements IResponse {
    private string $content;
    private int $statusCode;
    private array $headers;

    public function __construct(string $content = "", int $statusCode = 200, array $headers = []) {
        $this->content = $content;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function getContent(): string {
        return $this->content;
    }

    public function setContent(string $content): void {
        $this->content = $content;
    }

    public function getStatusCode(): int {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function getHeaders(): array {
        return $this->headers;
    }

    public function setHeader(string $key, string $value): void {
        $this->headers[$key] = $value;
    }

    public function send(): void {
        foreach ($this->headers as $key => $value) {
            header("{$key}: {$value}");
        }
        http_response_code($this->statusCode);
        echo $this->content;
    }
}


?>