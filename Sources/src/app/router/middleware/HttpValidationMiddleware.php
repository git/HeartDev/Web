<?php
namespace App\Router\Middleware;
use App\Router\Request\IRequest;
use Shared\Validation\Validator;

class RequestValidationMiddleware extends Middleware {
    private $validator;
    private $rules;

    public function __construct(Validator $validator, array $rules) {
        $this->validator = $validator;
        $this->rules = $rules;
    }

    public function handle(IRequest $request, callable $next) {
        $this->validateRequest($request);
        return parent::handle($request, $next);
    }

    private function validateRequest(IRequest $request) {
        foreach ($this->rules as $param => $ruleSet) {
            foreach ($ruleSet as $rule) {
                $this->validator->rule($param, $rule['callback'], $rule['message']);
            }
        }

        $requestData = array_merge($request->getQueryParameters(), $request->getRequestParameters());
        $this->validator->assert($requestData);
    }

}

// $validationRules = [
//     'email' => [
//         ['callback' => Validator::required(), 'message' => 'Email is required.'],
//         ['callback' => Validator::email(), 'message' => 'Email must be a valid email address.']
//     ],
//     // Add more rules as needed
// ];