<?php
namespace App\Router\Middleware;

use App\Router\Request\IRequest;

interface IHttpMiddleware {
    public function handle(IRequest $request, callable $next);
    
}