<?php

namespace App\Router\Middleware;

use App\Router\Request\IRequest;

abstract class Middleware implements IHttpMiddleware {
    protected $next;

    public function setNext(IHttpMiddleware $nextMiddleware) {
        $this->next = $nextMiddleware;
    }

    public function handle(IRequest $request, callable $next) {
        if ($this->next !== null) {
            return $this->next->handle($request, $next);
        }
        return $next($request);
    }
}
