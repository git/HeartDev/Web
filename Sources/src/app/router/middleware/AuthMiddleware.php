<?php

namespace App\Router\Middleware;
use App\Router\Session;
use Shared\Log;
use App\Router\Request\IRequest;
use App\Router\Response\RedirectResponse;
class AuthMiddleware extends Middleware {
    public function handle(IRequest $request, callable $next) {
        // if (isset($_SESSION['user'])) {
        //     $resp =new RedirectResponse("/");
        //     $resp->send();
        //     exit;
        // }
//         La page n’est pas redirigée correctement

// Firefox a détecté que le serveur redirige la demande pour cette adresse d’une manière qui n’aboutira pas.

//     La cause de ce problème peut être la désactivation ou le refus des cookies.
        // if (!isset($_SESSION['user'])) {
        //     $resp =new RedirectResponse("/log");
        //     $resp->send();
        //     exit;
        // }
        return parent::handle($request, $next);
    }
}