<?php
namespace App\Router\Middleware;

use App\Router\Request\IRequest;

class LoggingMiddleware extends Middleware {
    public function handle(IRequest $request, callable $next) {
        // Logique de journalisation
        echo "LoggingMiddleware: Log request - Method: {$request->getMethod()}, URI: {$request->getRequestUri()}\n";
        return parent::handle($request, $next);
    }
}