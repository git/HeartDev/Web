<?php
namespace App\Router\Request;

// should maybe change this
class ContentStrategyFactory {
    private static $strategyMap = [
        'application/json' => JsonContentStrategy::class,
        // Format...
    ];

    public static function createContentStrategy(string $contentType, string $requestMethod): ContentStrategy {
        foreach (self::$strategyMap as $type => $className) {
            if ($contentType === $type || in_array($requestMethod, ['PUT', 'PATCH', 'DELETE'])) {
                return new $className();
            }
        }
        return new FormContentStrategy();
    }

    public static function registerStrategy(string $contentType, string $className): void {
        self::$strategyMap[$contentType] = $className;
    }
}