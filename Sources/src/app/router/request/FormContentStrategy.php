<?php
namespace App\Router\Request;


class FormContentStrategy implements ContentStrategy {
    public function getContent(): array {
        return $_POST;
    }
}