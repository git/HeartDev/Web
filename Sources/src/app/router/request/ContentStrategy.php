<?php
namespace App\Router\Request;

interface ContentStrategy {
    public function getContent(): array;
}