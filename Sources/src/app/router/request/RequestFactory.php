<?php
namespace App\Router\Request;

class RequestFactory {

    public static function createFromGlobals(): IRequest {
        $query = $_GET;
        $server = $_SERVER;
        $headers = self::getRequestHeaders();

        $contentType = $headers['Content-Type'] ?? '';
        $contentStrategy = ContentStrategyFactory::createContentStrategy($contentType, $server['REQUEST_METHOD']);
        
        return new HttpRequest($query, $server, $headers, $contentStrategy,[]);
    }
    // should not be heare
    private static function getRequestHeaders(): array {
        $headers = [];
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
                $headers[$header] = $value;
            }
        }
        return $headers;
    }
}