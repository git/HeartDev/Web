<?php
namespace App\Router\Request;

class JsonContentStrategy implements ContentStrategy {
    public function getContent(): array {
        $rawContent = file_get_contents('php://input');
        return json_decode($rawContent, true) ?? [];
    }
}