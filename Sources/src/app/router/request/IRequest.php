<?php
namespace App\Router\Request;


interface IRequest
{
    public function getRequestUri();

    public function getBody();
    public function addToBody(string|array $attributes);

    public function getHeaders();
    public function getMethod();
    public function getQueryParameters(): array;
    public function getRequestParameters(): array;
}
