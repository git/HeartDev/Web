<?php
namespace App\Router\Request;

class HttpRequest implements IRequest {
    private $queryParameters;
    private $requestParameters;
    private $method;
    private $requestUri;
    private $headers;

    private array $body;

    public function __construct(
        array $query,
        array $server,
        array $headers,
        ContentStrategy $contentStrategy,
        array $body
            ) {
        $this->queryParameters = $query;
        $this->requestUri = $server['REQUEST_URI'] ?? '';
        $this->method = strtoupper($server['REQUEST_METHOD'] ?? 'GET');
        $this->headers = $headers;
        $this->requestParameters = $contentStrategy->getContent();
        $this->body = $body;
    }

    public function getQueryParameters(): array {
        return $this->queryParameters;
    }

    public function getRequestParameters(): array {
        return $this->requestParameters;
    }

    public function getMethod(): string {
        return $this->method;
    }

    public function getRequestUri(): string {
        return $this->requestUri;
    }

    public function getHeaders(): array {
        return $this->headers;
    }

    public function getBody(): array{
        return $this->body;
    }
    public function addToBody(string|array $attributes){
        $this->body[] = $attributes;
    }
    
}