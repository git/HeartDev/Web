document.getElementById('saveButton').addEventListener('click', function() {
    var preferences = {
        notifications: document.getElementById('notif').checked,
        theme: document.getElementById('theme').value
    };

    fetch('/savePreferences', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(preferences),
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch((error) => console.error('Error:', error));
});