<?php

namespace App\Views\Directives;
use App\Router\Router;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class Navigate extends AbstractExtension
{
    private Router $router;
    public function __construct( Router $router)
    {
        $this->router = $router;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('navigate', [$this, 'getPath']),
        ];
    }
    public function getPath(string $name, array $parameters = []): string
    {
        return $this->router->generate($name, $parameters);
    }



}