<?php

use Dotenv\Dotenv;
use Shared\Log;

$dotenv = Dotenv::createUnsafeImmutable(__DIR__,'.env');
$dotenv->safeLoad();
// echo($_ENV);
// apenrently getEnv is not a good thing cause
// const DB_HOST = $_ENV['DB_HOST'] ?? 'localhost';
// const DB_DATABASE = $_ENV['DB_DATABASE'] ?? 'heartTrack';
// const DB_USER = $_ENV['DB_USER'] ?? 'toto';
// const DB_PASSWORD = $_ENV['DB_PASSWORD'] ?? 'achanger';
define("APP_ENV", 'development');

const DB_HOST = 'localhost';
const DB_DATABASE = 'heartTrack';
const DB_USER = 'toto';
const DB_PASSWORD = 'achanger';
//const APP_ENV = 'console';

const DSN = "mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE;
