<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.html */
class __TwigTemplate_16e6c0dcab0136c1f1e8ae28198d95d31cd5a976b1114c0dd185f5b3a7465074 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta charset=\"utf-8\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />
        <meta name=\"description\" content=\"Accueil\" />
        <meta name=\"author\" content=\"PINAGOT Antoine\" />
        <title>Accueil - HeartTrack</title>
        <link href=\"https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css\" rel=\"stylesheet\" />
        <link href=\"../css/styles.css\" rel=\"stylesheet\" />
        <script src=\"https://use.fontawesome.com/releases/v6.3.0/js/all.js\" crossorigin=\"anonymous\"></script>
    </head>
    <body class=\"sb-nav-fixed\">
        <nav class=\"sb-topnav navbar navbar-expand navbar-dark bg-dark\">
            <!-- Navbar Brand-->
            <img class=\"navbar-brand ps-3\" src=\"../../../public/assets/img/logo.png\">
            <!-- Sidebar Toggle-->
            <button class=\"btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0\" id=\"sidebarToggle\" href=\"#!\"><i class=\"fas fa-bars\"></i></button>
            <!-- Navbar Search-->
            <form class=\"d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0\">
                <div class=\"input-group\">
                    <input class=\"form-control\" type=\"text\" placeholder=\"Rechercher...\" aria-label=\"Rechercher...\" aria-describedby=\"btnNavbarSearch\" />
                    <button class=\"btn btn-primary\" id=\"btnNavbarSearch\" type=\"button\"><i class=\"fas fa-search\"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class=\"navbar-nav ms-auto ms-md-0 me-3 me-lg-4\">
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle\" id=\"navbarDropdown\" href=\"#\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fas fa-user fa-fw\"></i></a>
                    <ul class=\"dropdown-menu dropdown-menu-end\" aria-labelledby=\"navbarDropdown\">
                        <li><a class=\"dropdown-item\" href=\"#!\">Profile</a></li>
                        <li><a class=\"dropdown-item\" href=\"#!\">Paramètres</a></li>
                        <li><hr class=\"dropdown-divider\" /></li>
                        <li><a class=\"dropdown-item\" href=\"#!\">Déconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id=\"layoutSidenav\">
            <div id=\"layoutSidenav_nav\">
                <nav class=\"sb-sidenav accordion sb-sidenav-dark\" id=\"sidenavAccordion\">
                    <div class=\"sb-sidenav-menu\">
                        <div class=\"nav\">
                            <div class=\"sb-sidenav-menu-heading\">Menu</div>
                            <a class=\"nav-link\" href=\"home.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/house.png\"></div>
                                Accueil
                            </a>
                            <div class=\"sb-sidenav-menu-heading\">Activités</div>
                            <a class=\"nav-link\" href=\"exercice.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/sprinter.png\"></div>
                                Exercices
                            </a>
                            <a class=\"nav-link\" href=\"analyze.html\">
                                <div class=\"sb-nav-link-icon\"><i class=\"fas fa-chart-area\"></i></div>
                                Analyses
                            </a>
                            <div class=\"sb-sidenav-menu-heading\">Social</div>
                            <a class=\"nav-link\" href=\"friend.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/group.png\"></div>
                                Amis
                            </a>
                            <a class=\"nav-link\" href=\"coaching.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/coaching.png\"></div>
                                Coaching
                            </a>
                            <a class=\"nav-link\" href=\"mail.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/letter.png\"></div>
                                Messagerie
                            </a>
                        </div>
                    </div>
                    <div class=\"sb-sidenav-footer\">
                        <div class=\"small\">Connecté en tant que:</div>
                        {user}
                    </div>
                </nav>
            </div>
            <div id=\"layoutSidenav_content\">
                <main>
                    <div class=\"container-fluid px-4\">
                        <h1 class=\"mt-4\">Accueil</h1>
                        <ol class=\"breadcrumb mb-4\">
                            <li class=\"breadcrumb-item active\">Vue d'ensemble</li>
                        </ol>
                        <div class=\"row\">
                            <div class=\"col-7\">
                                <div class=\"card mb-4\">
                                    <div class=\"card-header\">
                                        <i class=\"fas fa-chart-area me-1\"></i>
                                        Stastiques globales
                                    </div>
                                    <div class=\"card-body\">
                                        <canvas id=\"myAreaChart\" width=\"100%\" height=\"40\"></canvas>
                                    </div>
                                </div>
                            </div>
                            
                            <div class=\"col-5\">
                                <div class=\"card mb-4\">
                                    <div class=\"card-header\">
                                        <i class=\"fas fa-chart-bar me-1\"></i>
                                        Résumé quotidien
                                    </div>
                                    <div class=\"card-body\">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-5\">
                                <div class=\"card mb-4\">
                                    <div class=\"card-header\">
                                        <i class=\"fas fa-chart-bar me-1\"></i>
                                        Liste d'amis
                                    </div>
                                    <div class=\"card-body\">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-7\">
                                <div class=\"card mb-4\">
                                    <div class=\"card-header\">
                                        <i class=\"fas fa-chart-bar me-1\"></i>
                                        Messagerie
                                    </div>
                                    <div class=\"card-body\">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class=\"py-4 bg-light mt-auto\">
                    <div class=\"container-fluid px-4\">
                        <div class=\"d-flex align-items-center justify-content-between small\">
                            <div class=\"text-muted\">Copyright &copy; HeartTrack 2023</div>
                            <div>
                                <a href=\"#\">Politique de confidentialité</a>
                                &middot;
                                <a href=\"#\">Termes &amp; Conditions d'utilisations</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js\" crossorigin=\"anonymous\"></script>
        <script src=\"js/scripts.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js\" crossorigin=\"anonymous\"></script>
        <script src=\"assets/demo/chart-area-demo.js\"></script>
        <script src=\"assets/demo/chart-bar-demo.js\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js\" crossorigin=\"anonymous\"></script>
        <script src=\"js/datatables-simple-demo.js\"></script>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "home.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home.html", "/home/www/anpinagot/public_html/sae/Web/Sources/src/app/views/Templates/home.html");
    }
}
