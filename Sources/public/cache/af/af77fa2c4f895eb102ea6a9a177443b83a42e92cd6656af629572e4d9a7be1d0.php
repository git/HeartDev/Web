<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_83f313b86da35998a96ccc25fa02176c3fca11644e117af297089ca4a85f55ab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'menu' => [$this, 'block_menu'],
            'body' => [$this, 'block_body'],
            'script' => [$this, 'block_script'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr \">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "    
    ";
        // line 13
        $this->displayBlock('javascripts', $context, $blocks);
        // line 16
        echo "</head>
<body class=\"sb-nav-fixed\">
    ";
        // line 18
        $this->displayBlock('menu', $context, $blocks);
        // line 85
        echo "
    ";
        // line 86
        $this->displayBlock('body', $context, $blocks);
        // line 87
        echo "
    ";
        // line 88
        $this->displayBlock('script', $context, $blocks);
        // line 97
        echo "</body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "        <link href=\"../css/styles.css\" rel=\"stylesheet\" />
        <link href=\"https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css\" rel=\"stylesheet\" />
    ";
    }

    // line 13
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "        <script src=\"https://use.fontawesome.com/releases/v6.3.0/js/all.js\" crossorigin=\"anonymous\"></script>
    ";
    }

    // line 18
    public function block_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    <nav class=\"sb-topnav navbar navbar-expand navbar-dark bg-dark\">
            <!-- Navbar Brand-->
            <img class=\"navbar-brand ps-3\" src=\"../../../public/assets/img/logo.png\">
            <!-- Sidebar Toggle-->
            <button class=\"btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0\" id=\"sidebarToggle\" href=\"#!\"><i class=\"fas fa-bars\"></i></button>
            <!-- Navbar Search-->
            <form class=\"d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0\">
                <div class=\"input-group\">
                    <input class=\"form-control\" type=\"text\" placeholder=\"Rechercher...\" aria-label=\"Rechercher...\" aria-describedby=\"btnNavbarSearch\" />
                    <button class=\"btn btn-primary\" id=\"btnNavbarSearch\" type=\"button\"><i class=\"fas fa-search\"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class=\"navbar-nav ms-auto ms-md-0 me-3 me-lg-4\">
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle\" id=\"navbarDropdown\" href=\"#\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fas fa-user fa-fw\"></i></a>
                    <ul class=\"dropdown-menu dropdown-menu-end\" aria-labelledby=\"navbarDropdown\">
                        <li><a class=\"dropdown-item\" href=\"#!\">Profile</a></li>
                        <li><a class=\"dropdown-item\" href=\"#!\">Paramètres</a></li>
                        <li><hr class=\"dropdown-divider\" /></li>
                        <li><a class=\"dropdown-item\" href=\"#!\">Déconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id=\"layoutSidenav\">
            <div id=\"layoutSidenav_nav\">
                <nav class=\"sb-sidenav accordion sb-sidenav-dark\" id=\"sidenavAccordion\">
                    <div class=\"sb-sidenav-menu\">
                        <div class=\"nav\">
                            <div class=\"sb-sidenav-menu-heading\">Menu</div>
                            <a class=\"nav-link\" href=\"home.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/house.png\"></div>
                                Accueil
                            </a>
                            <div class=\"sb-sidenav-menu-heading\">Activités</div>
                            <a class=\"nav-link\" href=\"exercice.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/sprinter.png\"></div>
                                Exercices
                            </a>
                            <a class=\"nav-link\" href=\"analyze.html\">
                                <div class=\"sb-nav-link-icon\"><i class=\"fas fa-chart-area\"></i></div>
                                Analyses
                            </a>
                            <div class=\"sb-sidenav-menu-heading\">Social</div>
                            <a class=\"nav-link\" href=\"friend.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/group.png\"></div>
                                Amis
                            </a>
                            <a class=\"nav-link\" href=\"coaching.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/coaching.png\"></div>
                                Coaching
                            </a>
                            <a class=\"nav-link\" href=\"mail.html\">
                                <div class=\"sb-nav-link-icon\"><img src=\"assets/img/letter.png\"></div>
                                Messagerie
                            </a>
                        </div>
                    </div>
                    <div class=\"sb-sidenav-footer\">
                        <div class=\"small\">Connecté en tant que:</div>
                        {user}
                    </div>
                </nav>
            </div>
    ";
    }

    // line 86
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 88
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js\" crossorigin=\"anonymous\"></script>
    <script src=\"js/scripts.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js\" crossorigin=\"anonymous\"></script>
    <script src=\"assets/demo/chart-area-demo.js\"></script>
    <script src=\"assets/demo/chart-bar-demo.js\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js\" crossorigin=\"anonymous\"></script>
    <script src=\"js/datatables-simple-demo.js\"></script>
    ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  191 => 89,  187 => 88,  181 => 86,  112 => 19,  108 => 18,  103 => 14,  99 => 13,  93 => 9,  89 => 8,  83 => 6,  78 => 97,  76 => 88,  73 => 87,  71 => 86,  68 => 85,  66 => 18,  62 => 16,  60 => 13,  57 => 12,  55 => 8,  50 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/home/www/anpinagot/public_html/sae/Web/Sources/src/app/views/Templates/base.html.twig");
    }
}
