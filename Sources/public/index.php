<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/config.php';

use App\AppCreator;
use App\Router\Middleware\LoggingMiddleware;
use App\Router\Request\RequestFactory;
use Manager\UserManager;
use Manager\DataManager;

use Repository\IUserRepository;
use Shared\ArgumentControllerResolver;
use Shared\IArgumentResolver;
use Stub\AuthService;
use Stub\NotificationRepository;
use Stub\TrainingRepository;
use Stub\UserRepository;
use Stub\RelationshipRequestRepository;
use Repository\INotificationRepository;
use App\Router\Middleware\AuthMiddleware;





use Network\IAuthService;
use Network\IFriendRequestService;
use Network\RelationshipService;
use Network\INotificationService;
use Stub\NotificationService;


use Stub\StubData;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Shared\IHashPassword;
use Shared\HashPassword;


$appFactory = new AppCreator();
$appFactory->registerService(IArgumentResolver::class, ArgumentControllerResolver::class);
$appFactory->registerService(UserManager::class, UserManager::class);
$appFactory->registerService(DataManager::class, StubData::class);
$appFactory->registerService(IAuthService::class, AuthService::class);
$appFactory->registerService(IFriendRequestService::class, RelationshipService::class);
$appFactory->registerService(IHashPassword::class, HashPassword::class);
$appFactory->registerService(INotificationService::class, NotificationService::class);
$appFactory->registerService(INotificationRepository::class, NotificationRepository::class);
$appFactory->registerService(IUserRepository::class, UserRepository::class);







$appFactory->registerService(\Twig\Loader\LoaderInterface::class, function() {
    return new FilesystemLoader(__DIR__ . '/../src/app/views/Templates');
});

$appFactory->registerService(\Twig\Environment::class,\Twig\Environment::class);

// Connexion à la base de données
// $databaseContext = DatabaseContext::getInstance();

$appFactory->AddControllers();

