document.getElementById('saveButton').addEventListener('click', function() {
    var preferences = {
        notifications: document.getElementById('notif').checked,
        theme: document.getElementById('theme').value
    };

    fetch('/index.php/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(preferences),
    })
    .catch((error) => console.error('Error:', error));
});