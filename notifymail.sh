#!/bin/bash

# Mailjet API Credentials
API_KEY='MAILJET_API_KEY'
API_SECRET='MAILJET_API_SECRET'

#Mail info

SUBJECT='Nouvelle version démo'
changelog="$(base64 -w 0 CHANGELOG)"

FROM_EMAIL='contactHeartTrack@gmail.com'
FROM_NAME='Equipe de développement'

# This call sends a message to the given recipient with vars and custom vars.
curl -s \
    -X POST \
    --user "$API_KEY:$API_SECRET" \
    https://api.mailjet.com/v3.1/send \
    -H 'Content-Type: application/json' \
    -d '{ 
        "Messages":[ 
            {
            "From": { "Email": "$FROM_EMAIL", "Name": "$FROM_NAME" }, 
            "To": [ { "Email": "equipedev@waveheart.fr", "Name": "dev" } ],
            "TemplateID": 0000, "TemplateLanguage": true,
            "Subject": "Nouvelle version démo", "Variables": {},
            "Attachments": [ { "ContentType": "text/plain", "Filename": "changelog.md", "Base64Content": "'"$changelog"'" } ] } ] }'
