[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Cas d'utilisation : Gestion des Relations Sociales pour un Athlète

Bienvenue dans la sphère sociale de notre application dédiée aux athlètes ! Ce diagramme de cas d'utilisation explore les fonctionnalités sociales clés offertes aux utilisateurs, mettant en lumière la connectivité et l'interaction sociale au sein de notre communauté sportive.

**Acteurs Principaux :**

- **Athlète :** Le protagoniste central, utilisant l'application pour gérer ses relations sociales et explorer les profils de ses pairs.

**Fonctionnalités Clés :**

- **Ajouter un Ami :** Permet à l'athlète d'ajouter de nouveaux amis, nécessitant la saisie du nom de l'ami et soumis à une authentification préalable.

- **Supprimer un Ami :** Offre la possibilité de retirer un ami, exigeant une authentification pour garantir la sécurité des données.

- **Voir Mes Amis :** Permet à l'athlète de visualiser la liste de ses amis, avec la possibilité d'accéder à des fonctionnalités supplémentaires comme la visualisation des profils.

- **Modifier Mes Informations :** Offre à l'athlète la possibilité de mettre à jour ses informations personnelles et de connexion, avec des extensions pour des détails plus spécifiques.

**Flux d'Interaction :**

- Le processus d'ajout d'un ami inclut la saisie du nom de l'ami, tandis que la suppression d'un ami et la visualisation de la liste des amis sont soumises à une authentification préalable pour protéger la confidentialité.

- Les modifications d'informations englobent deux extensions : la mise à jour des informations personnelles et la mise à jour des informations de connexion, offrant une personnalisation approfondie du profil athlétique.

- La visualisation du profil d'un ami s'étend à des fonctionnalités telles que la consultation des activités et des statistiques de l'ami, ajoutant une dimension sociale à l'expérience de suivi sportif.

Explorez ce diagramme pour découvrir comment notre application encourage l'interaction sociale entre les athlètes, favorisant une communauté engagée et collaborative au sein de laquelle les utilisateurs peuvent partager, interagir et se soutenir mutuellement dans leur parcours sportif.


```plantuml
left to right direction
:Athlete: as a

a --> (Ajouter un ami)
a --> (Supprimer un ami)
a --> (Voir mes amis)
a --> (Modifier mes informations)
(Ajouter un ami)->(Saisir le nom de l'ami)
(Supprimer un ami)..>(S'authentifier) : <<include>>
(Ajouter un ami)..>(S'authentifier) : <<include>>
(Voir mes amis)..>(S'authentifier) : <<include>>
(Voir mes amis)..>(Lister les amis) : <<include>>
(Modifier mes informations)..>(Informations personnelles) : <<extends>>
(Modifier mes informations)..>(Informations de connexion) : <<extends>>
(Modifier mes informations)..>(S'authentifier) : <<include>>
(Lister les amis)..>(Voir son profil) : <<include>>
(Voir son profil)..>(Voir ses activités) : <<extends>>
(Voir son profil)..>(Voir ses statistiques) : <<extends>>
(S'authentifier)..>(S'inscrire) : <<extends>>
(S'authentifier)..>(Se connecter) : <<include>>
(S'inscrire)..>(Inscription Athlète) : <<include>>
```