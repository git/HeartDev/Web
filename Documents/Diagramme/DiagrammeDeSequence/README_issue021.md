[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Processus de Connexion sur la Plateforme

Bienvenue sur notre plateforme de gestion d'activités sportives ! Pour offrir une expérience fluide et sécurisée, nous avons mis en place un processus de connexion intuitif. Découvrez comment accéder à votre compte ou créer un nouveau compte en quelques étapes simples.

**Étapes du Processus :**

1. **Demande de Page de Connexion :** L'utilisateur démarre en exprimant le désir de se connecter à la plateforme.

2. **Vérification de la Connexion Préexistante :** Le système vérifie si l'utilisateur est déjà connecté. En cas de connexion active, l'utilisateur est redirigé directement vers sa page de compte.

3. **Page de Connexion :** Si l'utilisateur n'est pas encore connecté, il est dirigé vers la page de connexion, où il peut saisir ses informations d'identification.

4. **Choix pour les Utilisateurs Possédant un Compte :** Si l'utilisateur a déjà un compte, il peut fournir ses informations de connexion existantes.

5. **Création de Compte pour les Nouveaux Utilisateurs :** Pour ceux qui n'ont pas encore de compte, l'option de création de compte est disponible. L'utilisateur peut fournir les détails nécessaires pour créer son compte.

6. **Page de Création de Compte :** Une page dédiée guide l'utilisateur tout au long du processus de création de compte, lui permettant de saisir les informations nécessaires.

7. **Validation et Connexion :** Une fois que les informations de connexion ou de création de compte sont fournies, le système procède à la vérification et connecte l'utilisateur à son compte.


```plantuml
actor User as u
u->Systeme : demandePageConnexion()
alt User déjà connecté
   Systeme-->u : redirectionPageCompte()   
end
Systeme-->u : PageConnexion()
alt User possède déjà un compte
   u->Systeme:InfosConnexion()
else 
   u->Systeme:CreerCompte()
   Systeme-->u :PageCreationCompte()
   u->Systeme:InfosCreationCompte()
end
Systeme-->u :Connecter()
```