[retour au README.md](../../README.md)  
[Retour aux Documents](../README_DOCUMENTS.md)

# Diagrammes nécéssaires à notre projet

## Diagrammes de classes
- [issue016  - Statistiques coach ](DiagrammeDeClasses/README_issue016.md)
- [issue022  - Ajout des amis ](DiagrammeDeClasses/README_issue022.md)
- [issue023  - User Gateway ](DiagrammeDeClasses/README_issue023.md)
- [issue028 - Importation de fichiers .fit](DiagrammeDeClasses/README_issue028.md)
- [couche d'accès aux données](DiagrammeDeClasses/README_accesDonnees.md)
- [Diagramme général](DiagrammeDeClasses/README_DIAGRAMME.md)

## Diagrammes de séquence
- [Envoi de demande d'ami](DiagrammeDeSequence/README_demandeAmi.md)
- [Accepter une demande d'ami](DiagrammeDeSequence/README_accepterAmi.md)
- [Supprimer un ami](DiagrammeDeSequence/README_suppressionAmi.md)
- [issue021  - Authentification ](DiagrammeDeSequence/README_issue021.md)

## Diagrammes de cas d'utilisation
- [Cas d'utilisation pour la gestion du compte et des amitiés](CasUtilisations/README_gestionCompteAmitie.md)
- [Cas d'utilisation pour la gestion des activités et données](CasUtilisations/README_gestionActivites.md)
- [Cas d'utilisation pour la suivi d'une équipe sportive](CasUtilisations/README_coachSuiviSportif.md)

## Base de Données
- [MLD](BDD/README_BDD.md)