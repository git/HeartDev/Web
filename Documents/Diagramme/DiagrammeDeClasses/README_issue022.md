[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md) 

# Diagramme de Classes : Gestion des Utilisateurs et Notifications

Bienvenue dans le cœur de notre système, où la gestion des utilisateurs et des notifications prend vie à travers ce diagramme de classes. Explorez les relations et les fonctionnalités essentielles qui orchestrent l'interaction entre les utilisateurs, les demandes d'amis, et les notifications.

**Entités Principales :**

- **Utilisateur (User) :** Représente les individus inscrits sur notre plateforme, caractérisés par leur nom et établissant des liens d'amitié avec d'autres utilisateurs.
  
- **Notification (Notification) :** Contient le texte informatif des notifications qui peuvent être émises par le système.

- **Demande d'Ami (Ask) :** Modélise une demande d'amitié émise par un utilisateur en direction d'un autre.

**Interfaces et Classes Abstraites :**

- **INotifier :** Interface définissant la méthode `notify()`, implémentée par des classes concrètes pour gérer la notification aux observateurs.

- **Observer :** Interface définissant la méthode `update()`, implémentée par les classes qui souhaitent être informées des changements dans un sujet observé.

- **UserManager :** Classe abstraite gérant la logique métier liée aux utilisateurs, tels que l'ajout ou la suppression d'amis, la réponse aux demandes d'amis, et la récupération de la liste d'amis.

- **IUserRepository :** Interface définissant les méthodes pour la recherche d'utilisateurs et l'ajout d'un nouvel utilisateur.

**Relations Clés :**

- Les utilisateurs peuvent avoir plusieurs amis et plusieurs notifications.

- La classe UserManager est connectée à IUserRepository pour gérer les opérations liées aux utilisateurs.

- Observer et Subject sont des composants du modèle de conception "Observer", permettant la notification efficace des changements dans le système.

Plongez-vous dans ce diagramme pour découvrir comment notre application crée un écosystème social dynamique, permettant aux utilisateurs d'interagir, de rester informés et de développer des liens significatifs au sein de la communauté.


```plantuml
class User {
    + name : string
}

User "1" --> "*" User: friends
User "1" --> "*" Notification: notifications
User "1" --> "*" Ask: friendRequests
class Notification {
    - text : string
}

interface INotifier {
 + notify() : void
}

INotifier --|> Observer

abstract class UserManager {
    - currentUser : User
    + deleteFriend(userId : int) : void
    + addFriend(userId : int) : void
    + respondToFriendRequest(requestId : int, choice : bool) : void
    + getFriends(userId : int) : User[]
}

class Ask {
    - fromUser : int
    - toUser : int
}

Ask --|> Subject

abstract class Subject {
    + attach(o : Observer) : void
    + detach(o : Observer) : void
    + notify() : void
}

Subject "1" --> "*" Observer
interface Observer {
    + update() : void
}

UserManager ..> User
UserManager o-- IUserRepository
UserManager o-- INotifier

interface IUserRepository {
    + findByUsername(username : string) : User
    + addUser(user : User) : bool
}

IUserRepository ..> User
```