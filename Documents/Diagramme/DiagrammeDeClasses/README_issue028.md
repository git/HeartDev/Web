[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Diagramme de classes pour l'importation de fichiers .fit

Bienvenue dans le monde de la gestion d'activités sportives avec notre application innovante ! Ce diagramme de classe se concentre sur une fonctionnalité essentielle qui améliorera l'expérience des utilisateurs : l'importation de fichiers .fit. Nous avons conçu un diagramme de classes pour vous offrir une vision claire et structurée de la manière dont cette fonctionnalité est implémentée au sein de notre application.

**Acteurs Principaux :**

- Utilisateur (User) : Représente un individu inscrit sur notre plateforme.
- Athlète (Athlete) : Un type spécialisé d'utilisateur, bénéficiant de fonctionnalités supplémentaires liées à la gestion d'activités sportives et avec la capacité d'importer des fichiers .fit.  

**Entités Clés :**

- Activité (Activity) : Représente une session d'activité physique, avec des détails tels que le type, la date, la durée, et plus encore.
- Gestionnaires (Managers) : Gérant différentes facettes de l'application, notamment les utilisateurs, les activités et les fichiers.

**Fonctionnalité Clé :**

- Importation de fichiers .fit : Permet aux utilisateurs de charger des données provenant de fichiers .fit via la bibliothèque `php-fit-file-analysis`, générés par des dispositifs de suivi d'activité. Ces fichiers contiennent des informations précieuses telles que la fréquence cardiaque, la distance parcourue et d'autres données de santé importante pour nos analyses.

**Architecture :**

- AuthService (Service d'Authentification) : Gère l'authentification des utilisateurs, garantissant un accès sécurisé à la fonction d'importation.
- UserManager (Gestionnaire d'Utilisateurs) : Gère les opérations liées aux utilisateurs, y compris l'importation de fichiers .fit.
- ActivityManager (Gestionnaire d'Activités) : Responsable du stockage et de la gestion des activités importées.

**Objectif :**

Offrir aux utilisateurs, en particulier aux athlètes, la possibilité d'enrichir leur profil et de suivre leur performance en important des données détaillées à partir de fichiers .fit.


```plantuml
@startuml issue028_DiagrammeDeClasses
class Activite {
    -idActivite:int
    -type:String
    -date:Date
    -heureDebut:Date
    -heureFin:Date
    -effortRessenti:int
    -variability:float
    -variance:float
    -standardDeviation:float
    -average:float
    -maximum:int
    -minimum:int
    -avrTemperature:float
    -hasAutoPause:boolean
    +getIdActivite():int
    +getType():String
    +getDate():Date
    +getHeureDebut():Date
    +getHeureFin():Date
    +getEffortRessenti():int
    +getVariability():float
    +getVariance():float
    +getStandardDeviation():float
    +getAverage():float
    +getMaximum():int
    +getMinimum():int
    +getAvrTemperature():float
    +setType(type:String):void
    +setEffortRessenti(effortRessenti:int):void
    +__toString():String
}
class Role {
    -id:int
}
class Athlete {
    +getActivities():array
    +addActivity(myActivity:Activity):boolean
}
class User {
    -id:int
    -username:String
    -nom:String
    -prenom:String
    -email:String
    -motDePasse:String
    -sexe:String
    -taille:float
    -poids:float
    -dateNaissance:Date
    +getId():int
    +setId(id:int):void
    +getUsername():String
    +setUsername(username:String):void
    +getNom():String
    +setNom(nom:String):void
    +getPrenom():String
    +setPrenom(prenom:String):void
    +getEmail():String
    +setEmail(email:String):void
    +getMotDePasse():String
    +setMotDePasse(motDePasse:String):void
    +getSexe():String
    +setSexe(sexe:String):void
    +getTaille():float
    +setTaille(taille:float):void
    +getPoids():float
    +setPoids(poids:float):void
    +getDateNaissance():Date
    +setDateNaissance(dateNaissance:Date):void
    +getRole():Role
    +setRole(role:Role):void
    +isValidPassword(password:String):boolean
    +__toString():String
}
class AthleteManager {
    +getActivities():array
}
class ActivityManager {
    +saveFitFileToJSON(monFichierFit:object):boolean
    +uploadFile(type:string, effortRessenti:int, file_path_or_data:string|resource, options:array):boolean
}
class DataManager {
}
class UserManager {
    +login(loginUser:string, passwordUser:string):boolean
    +register(loginUser:string, passwordUser:string, data:array):boolean
    +deconnecter():boolean
}

User -> Role: role
Athlete -|> Role
DataManager -> UserManager: -userMgr
DataManager -> AthleteManager: -athleteMgr
DataManager -> ActivityManager: -activityMgr
UserManager -> AuthService: -authService
UserManager -> User: -currentUser
ActivityManager -> AuthService: -authService
Athlete -> Activite: listActivite
AthleteManager -> AuthService: -authService
@enduml
```