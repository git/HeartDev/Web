[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md) 

# Introduction au Modèle de Données de l'Application

L'architecture de données de notre application de suivi d'activités sportives repose sur un modèle robuste, avec des entités clés pour représenter les activités, les athlètes et les coachs et la récupération de ces données au sein de notre application. Découvrez les composants principaux de notre modèle de données :

## Activité
L'entité Activité représente une session d'activité sportive avec des détails variés tels que le type d'activité, la date, la durée, l'effort ressenti, etc. Le `ActiviteEntity` encapsule ces données, tandis que le `ActiviteGateway` gère la communication avec la base de données pour les activités.

## Athlète
L'entité Athlète représente un utilisateur de l'application qui participe à des activités sportives. Le `AthleteEntity` stocke les détails de l'athlète, et le `AtheletGateway` facilite l'accès et la gestion des données des athlètes.

## Coach
L'entité Coach représente un utilisateur qui peut superviser et coacher d'autres athlètes. Le `CoachEntity` stocke les détails du coach, tandis que le `CoachGateway` gère les interactions avec la base de données.

## Mapper
Les mappers, tels que `ActiviteMapper`, `AthleteMapper`, et `CoachMapper`, facilitent la conversion entre les entités et les modèles utilisés dans l'application.

## Connexion à la Base de Données
La classe `Connection` étend de `PDO` et assure la connexion à la base de données. Chaque Gateway utilise cette connexion pour interagir avec la base de données.


```plantuml
@startuml
class ActiviteEntity {
    - idActivite: int
    - type: string
    - date: string
    - heureDebut: string
    - heureFin: string
    - effortRessenti: int
    - variabilite: int
    - variance: int
    - ecartType: int
    - moyenne: int
    - maximum: int
    - minimum: int
    - temperatureMoyenne: int
    + getIdActivite(): int
    + getType(): string
    + getDate(): string
    + getHeureDebut(): string
    + getHeureFin(): string
    + getEffortRessenti(): int
    + getVariabilite(): int
    + getVariance(): int
    + getEcartType(): int
    + getMoyenne(): int
    + getMaximum(): int
    + getMinimum(): int
    + getTemperatureMoyenne(): int
    + setIdActivite(idActivite: int): void
    + setType(type: string): void
    + setDate(date: string): void
    + setHeureDebut(heureDebut: string): void
    + setHeureFin(heureFin: string): void
    + setEffortRessenti(effortRessenti: int): void
    + setVariabilite(variabilite: int): void
    + setVariance(variance: int): void
    + setEcartType(ecartType: int): void
    + setMoyenne(moyenne: int): void
    + setMaximum(maximum: int): void
    + setMinimum(minimum: int): void
    + setTemperatureMoyenne(temperatureMoyenne: int): void
}
class ActiviteGateway {
    + __construct(connection: Connection)
    + getActivite(): ?array
    + getActiviteById(activiteId: int): ?array
    + getActiviteByType(type: string): ?array
    + getActiviteByDate(date: string): ?array
    + getActiviteByTimeRange(startTime: string, endTime: string): ?array
    + getActiviteByEffort(effortRessenti: int): ?array
    + getActiviteByVariability(variabilite: int): ?array
    + getActiviteByTemperature(temperatureMoyenne: int): ?array
    + addActivite(activite: ActiviteEntity): bool
    + updateActivite(oldActivite: ActiviteEntity, newActivite: ActiviteEntity): bool
    + deleteActivite(idActivite: int): bool
}
class ActiviteMapper {
    + map(data: array): ActiviteEntity
    + ActiviteEntityToModel(activiteEntity: ActiviteEntity): Activite
}
class AthleteEntity {
    - idAthlete: int
    - nom: string
    - prenom: string
    - email: string
    - sexe: string
    - taille: float
    - poids: float
    - motDePasse: string
    - dateNaissance: string
    + getIdAthlete(): int
    + getNom(): string
    + getPrenom(): string
    + getEmail(): string
    + getSexe(): string
    + getTaille(): float
    + getPoids(): float
    + getMotDePasse(): string
    + getDateNaissance(): string
    + setIdAthlete(idAthlete: int): void
    + setNom(nom: string): void
    + setPrenom(prenom: string): void
    + setEmail(email: string): void
    + setSexe(sexe: string): void
    + setTaille(taille: float): void
    + setPoids(poids: float): void
    + setMotDePasse(motDePasse: string): void
    + setDateNaissance(dateNaissance: string): void
}
class AtheletGateway {
    + __construct(connection: Connection)
    + getAthlete(): ?array
    + getAthleteById(userId: int): ?array
    + getAthleteByName(name: string): ?array
    + getAthleteByFirstName(firstName: string): ?array
    + getAthleteByEmail(email: string): ?array
    + getAthleteByGender(gender: string): ?array
    + getAthleteByHeight(height: int): ?array
    + getAthleteByWeight(weight: int): ?array
    + getAthleteByBirthDate(birthdate: string): ?array
    + addAthlete(athlete: AthleteEntity): bool
    + updateAthlete(oldAthlete: AthleteEntity, newAthlete: AthleteEntity): bool
    + deleteAthlete(idAthlete: int): bool
}
class AthleteMapper {
    + fromSqlToEntity(data: array): array
    + athleteEntityToModel(athleteEntity: AthleteEntity): User
    + athleteToEntity(user: User): AthleteEntity
}
class CoachEntity {
    - idCoach: int
    - nom: string
    - prenom: string
    - email: string
    - sexe: string
    - taille: float
    - poids: float
    - motDePasse: string
    - dateNaissance: string
    + getIdCoach(): int
    + getNom(): string
    + getPrenom(): string
    + getEmail(): string
    + getSexe(): string
    + getTaille(): float
    + getPoids(): float
    + getMotDePasse(): string
    + getDateNaissance(): string
    + setIdCoach(idCoach: int): void
    + setNom(nom: string): void
    + setPrenom(prenom: string): void
    + setEmail(email: string): void
    + setSexe(sexe: string): void
    + setTaille(taille: float): void
    + setPoids(poids: float): void
    + setMotDePasse(motDePasse: string): void
    + setDateNaissance(dateNaissance: string): void
}
class CoachGateway {
    + __construct(connection: Connection)
    + getCoach(): ?array
    + getCoachById(userId: int): ?array
    + getCoachByName(name: string): ?array
    + getCoachByFirstName(firstName: string): ?array
    + getCoachByEmail(email: string): ?array
    + getCoachByGender(gender : string): ?array
    + getCoachByHeight(height: int): ?array
    + getCoachByBirthDate(birthdate: string): ?array
    + addCoach(coach: CoachEntity): bool
    + updateCoach(oldCoach: CoachEntity, newCoach: CoachEntity): bool
    + deleteCoach(idCoach: int): bool
}
class CoachMapper {
    + map(data: array): CoachEntity
    + CoachEntityToModel(coachEntity: CoachEntity): User
    + CoachToEntity(user: User): CoachEntity
}
class Connection extends PDO {
    - stmt
    + __construct(dsn: string, username: string, password: string)
    + executeQuery(query: string, parameters: array): bool
    + executeWithErrorHandling(query: string, params: array): array
    + getResults(): array
}

Connection <- ActiviteGateway : connection
Connection <- AtheletGateway : connection
Connection <- CoachGateway : connection
AthleteMapper -> AthleteEntity
CoachMapper -> CoachEntity
ActiviteMapper -> ActiviteEntity
ActiviteMapper -> ActiviteGateway
CoachMapper -> CoachGateway
AthleteMapper -> AtheletGateway
@enduml
```