[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Diagramme de Classes : Plateforme de Gestion d'Activités Sportives

Bienvenue dans l'écosystème dynamique de notre plateforme de gestion d'activités sportives ! Ce diagramme de classes offre une vision complète des entités et des relations qui façonnent l'expérience des utilisateurs au sein de notre système.

**Entités Principales :**

- **Utilisateur (User) :** Représente les individus inscrits sur notre plateforme, avec des détails personnels tels que le nom, le prénom, l'email, etc. Chaque utilisateur a un rôle spécifique (Athlete, Coach) qui détermine ses interactions au sein de l'application.

- **Rôle (Role) :** Classe abstraite qui définit les rôles spécifiques des utilisateurs (Athlete, Coach). Contient des méthodes pour gérer les amis, les entraînements, et les demandes.

- **Athlète (Athlete) :** Spécialisation de la classe Role, représentant les utilisateurs actifs qui enregistrent des activités sportives, des statistiques, et interagissent avec d'autres athlètes.

- **Activité (Activite) :** Contient des détails sur une activité sportive tels que le type, la date, la durée, la fréquence cardiaque, etc.

- **Notification (Notification) :** Messages pour informer les utilisateurs des actions importantes.

- **Entraînement (Entrainement) :** Sessions planifiées d'activités physiques avec des détails comme la date, la localisation, la description, et les retours.

- **Statistique (Statistique) :** Informations détaillées sur les performances sportives d'un athlète, comprenant la distance totale, le poids, le temps total, la fréquence cardiaque, etc.

- **Source de Données (SourceDonnees) :** Représente les sources utilisées pour collecter des données, telles que les montres connectées.

**Relations Clés :**

- Les Utilisateurs ont un rôle spécifique (Athlete, Coach) qui détermine leurs fonctionnalités.

- Un Athlète peut enregistrer plusieurs Activités, possède des Statistiques, et une Sources de Données qui est la plus utilisé.

- Les Entraînements sont liés aux Utilisateurs, permettant une planification efficace.

- Les Notifications informent les Utilisateurs des événements importants tels qu'une demande d'amis ou une notification d'avertissement de ban.

Explorez ce diagramme pour comprendre comment notre plateforme offre une expérience complète, de la gestion des utilisateurs à l'enregistrement des activités sportives et au suivi des performances.


```plantuml
@startuml
class User {
    - id: int
    - username: String
    - nom: string
    - prenom: string
    - email: string
    - motDePasse: string
    - sexe: string
    - taille: float
    - poids: float
    - dateNaissance: \DateTime
    + getId(): int
    + getUsername(): string
    + setUsername(string $username): void
    + setId(int $id): void
    + getNom(): string
    + setNom(string $nom): void
    + getPrenom(): string
    + setPrenom(string $prenom): void
    + getEmail(): string
    + setEmail(string $email): void
    + getMotDePasse(): string
    + setMotDePasse(string $motDePasse): void
    + getSexe(): string
    + setSexe(string $sexe): void
    + getTaille(): float
    + setTaille(float $taille): void
    + getPoids(): float
    + setPoids(float $poids): void
    + getDateNaissance(): \DateTime
    + setDateNaissance(\DateTime $dateNaissance): void
    + getRole(): Role
    + setRole(Role $role): void
    + addNotification($notification): void
    + deleteNotification($index): void
    + isValidPassword(string $password): bool
    + __toString(): string
}
abstract class Role {
    - id: int
    - usersRequests: array
    + getUsersList(): array
    + getUsersRequests(): array
    + addUsersRequests(RelationshipRequest $request): void
    + removeRequest(RelationshipRequest $req): bool
    + CheckAdd(User $user): bool
    + addUser(User $user): bool
    + removeUser(User $user): bool
    + addTraining(Training $training): bool
    + getTrainingsList(): array
}
abstract class Coach extends Role {
}
class CoachAthlete extends Coach {
    + CheckAdd(User $user): bool
}
class Athlete extends Role {
    + getActivities(): array
    + addActivity(Activity $myActivity): bool
    + CheckAdd(User $user): bool
}
class Activite {
    - idActivity: int
    - type: String
    - date: \DateTime
    - heureDebut: \DateTime
    - heureFin: \DateTime
    - effortRessenti: int
    - variability: float
    - variance: float
    - standardDeviation: float
    - average: int
    - maximum: int
    - minimum: int
    - avrTemperature: float
    - hasAutoPause: bool
    + getIdActivity(): int
    + getType(): String
    + getDate(): \DateTime
    + getHeureDebut(): \DateTime
    + getHeureFin(): \DateTime
    + getEffortRessenti(): int
    + getVariability(): float
    + getVariance(): float
    + getStandardDeviation(): float
    + getAverage(): float
    + getMaximum(): int
    + getMinimum(): int
    + getAvrTemperature(): float
    + setType(String $type): void
    + setEffortRessenti(int $effortRessenti): void
    + __toString(): String
}
class Notification {
    - type: string
    - message: string
    - toUserId: int
    + getType(): string
    + setType(string $type): void
    + getMessage(): string
    + setMessage(string $message): void
    + getToUserId(): int
    + setToUserId(int $toUserId): void
    + __construct(int $toUserId,string $type, string $message)
    + __toString(): string
}
class Entrainement {
    - idTraining: int
    - date: \DateTime
    - latitude: float
    - longitude: float
    - description: String
    - feedback: String
    + getId(): int
    + getDate(): \DateTime
    + getLocation(): String
    + getDescription(): String
    + getFeedback(): String
    + __toString(): String
}
class Statistique {
    - idStat: int
    - distanceTotale: float
    - poids: float
    - tempsTotal: time
    - FCmoyenne: int
    - FCmin: int
    - FCmax: int
    - cloriesBrulees: int
    + getIdStat(): int
    + getDistanceTotale(): float
    + getPoids(): float
    + getTempsTotal(): time
    + getFCmoyenne(): int
    + getFCmin(): int
    + getFCmax(): int
    + getCloriesBrulees(): int
    + __toString(): String
}
class SourceDonnees {
    - idSource: int
    - nom: String
    - type: String
    - precision: enum
    - dateDerniereUtilisation: \DateTime
    + getIdSource(): int
    + getNom(): String
    + getType(): String
    + getPrecision(): enum
    + getDateDerniereUtilisation(): \DateTime
    + __toString(): String
}
User -> Role : role
Role -> User : usersList
Athlete -> Statistique : statsList
Athlete -> Activite : activityList
Athlete -> SourceDonnees : sdList
User -> Notification : notificationList
User -> Entrainement : trainingsList
Activite -> SourceDonnees : maSource
@enduml
```