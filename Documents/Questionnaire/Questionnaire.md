À qui est destiné l’application ?

Comment va-t-on récupérer les données ? Sous quel format ?

Utiliserons-nous une montre Open-Source ou un autre type de ressources pour récupérer les données ?

Comment voulez-vous traiter les données ?

A-t-on des capteurs géographiques pour récupérer les données des efforts à l’arrêt/en continu ?

Avez-vous des références de site/application pour nous aider dans la recherche de l’interface ?