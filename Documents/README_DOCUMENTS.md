[README.md](../README.md)
## SOMMAIRE
# Diagrammes
- [Diagrammes](Diagramme/README_DIAGRAMMES.md)

# Personnas
- [Personnas](Personnas/README_PERSONNAS.md)


# Gestion de projet
- [Gestion de projet](Gestion/README_GESTION.md)
