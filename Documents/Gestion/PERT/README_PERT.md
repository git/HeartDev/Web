[retour au README.md](../../../README.md)  
[Retour aux Documents](../../README_DOCUMENTS.md)  
[Retour à la Gestion](../README_GESTION.md)

## PERT
- [PERT - Excel](PERT.xlsx)

### PERT - Image
<img src="PERT1.png">
<img src="PERT2.png">