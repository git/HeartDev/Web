<div align = center>

  <h1>HeartTrack</h1>
  <img src="Documents/Images/logo.png" />
    
</div>



<div align = center>



---
&nbsp; ![PHP](https://img.shields.io/badge/PHP-000?style=for-the-badge&logo=Php&logoColor=white&color=purple)
&nbsp; ![CSS](https://img.shields.io/badge/CSS-000?style=for-the-badge&logo=css3&logoColor=white&color=darkblue)
&nbsp; ![DOCKER](https://img.shields.io/badge/Docker-2496ED.svg?style=for-the-badge&logo=Docker&logoColor=white)
&nbsp; ![PostgreSQL](https://img.shields.io/badge/Postgresql-000?style=for-the-badge&logo=postgresql&logoColor=white&color=blue)
&nbsp; ![JAVASCRIPT](https://img.shields.io/badge/JavaScript-000?style=for-the-badge&logo=javascript&logoColor=white&color=yellow)
</br>  
<!-- [![Build Status]()](https://codefirst.iut.uca.fr/git/HeartDev/Web)
[![Quality Gate Status]()](https://codefirst.iut.uca.fr/git/HeartDev/Web)
[![Bugs]()](https://codefirst.iut.uca.fr/git/HeartDev/Web)
[![Coverage]()](https://codefirst.iut.uca.fr/git/HeartDev/Web)
[![Vulnerabilities]()](https://codefirst.iut.uca.fr/git/HeartDev/Web)   -->

</div>  
  
# Table des matières
[Présentation](#présentation) | [Répartition du Git](#répartition-du-git) | [Documentation](#documentation) | [Prerequisites](#prerequisites) | [Getting Started](#getting-started) | [Features](#features) | [Ce que nous avons fait](#ce-que-nous-avons-fait) | [Fabriqué avec](#fabriqué-avec) | [Contributeurs](#contributeurs) | [Comment contribuer](#comment-contribuer) | [License](#license) | [Remerciements](#remerciements)



## Présentation

**Nom de l'application :** HeartTrack

### Contexte

HeartTrack est une application web PHP et mobile Android destinée aux sportifs et aux coachs afin de permettre l'analyse de courbes de fréquences cardiaques et le suivi d'équipe sportive. L'objectif principal de cette application est de récupérer les données de fréquence cardiaque à partir de fichiers .FIT, de les afficher sous forme de courbes, d'identifier des paternes, de fournir des statistiques et de réaliser des prédictions liées à l'effort physique, à la chaleur, à la récupération, etc.

### Récapitulatif du Projet

Le projet HeartTrack, avec son application HeartTrack, vise à offrir une solution Open Source d'analyse des données de fréquence cardiaque, en mettant l'accent sur les besoins des sportifs et des coachs. L'application sera capable de traiter et d'interpréter les données de manière intelligente, fournissant ainsi des informations précieuses pour optimiser les performances sportives et la santé.


## Répartition du Git

[**Sources**](Sources/) : **Code de l'application**

[**Documents**](Documents/README_DOCUMENTS.md) : **Documentation de l'application et diagrammes**

[**Wiki**](https://codefirst.iut.uca.fr/git/HeartDev/Web/wiki/PHP) : **Wiki de notre projet (attendus PHP)**

---

Le projet HeartTrack utilise un modèle de flux de travail Git (Gitflow) pour organiser le développement. Voici une brève explication des principales branches :

- **branche prod** : Cette branche contient la version de production stable de l'application. Les modifications sur cette branche sont généralement destinées à des mises en production.

- **branche master** : La branche master est similaire à la branche de production, mais elle peut contenir des fonctionnalités en cours de développement qui sont presque prêtes pour une mise en production.

- **branche test** : Cette branche est utilisée pour permettre différents tests sur l'application.

- **branche issue** : Pour chaque problème (issue) que vous résolvez, vous devez créer une branche portant le nom de l'issue, par exemple, "issue_#32_nom" où 32 est le numéro de l'issue et nom est une description courte de l'issue. Une fois l'issue résolue, assurez-vous de mettre à jour le changelog et de créer une merge request.
- **branche démo** : Cette branche est utilisée pour déployer une version démo de l'application. Elle est mise à jour avec les dernières fonctionnalités en développement.

## Documentation 
Documentation et informations à propos de `HearthTrack` disponible [ici]()

### Prerequisites
* [Visual Studio code](https://code.visualstudio.com/) - exemple d'IDE gratuit
* [Git](https://git-scm.com/) - Versioning
* [XAMPP : X, Apache, MySQL, Perl, PHP](https://www.apachefriends.org/fr/index.html) - Languages
* [PostgreSQL](https://www.postgresql.org/) - Base de Donnée
  
## Getting Started
1. Cloner le répos
2. Faire un `composer install` dans le dossier `Sources/`
3. Importer la base de données `Sources/Database/hearttrack.sql`
4. Lancer le serveur Apache et MySQL
5. Lancer le projet avec `php -S localhost:8000 -t Sources/`
6. Ouvrir le navigateur et aller sur `localhost:8000/index.php`
 
## Features
* [x] Import de fichier .fit
* [x] Ajout d'amis
* [x] Création de compte
* [x] Ajout d'athlete pour le coach
* [x] Création d'entrainement
* [x] Création de courbes
* [x] Gérer ses permissions

## Ce que nous avons fait
* [x] PDO et Pattern Gateway
* [x] implémentation MVC et 2 contrôleurs
* [x] pattern Frontcontroleur sans routage
* [x] autoloader simple sans namespace ?
* [x] validation des entrées
* [x] vues dont vue erreur
* [x] partie administration ou équivalent
* [x] vues complètes bien segmentées et utilisation bootstrap
* [x] utilisation namespace et psr4
* [x] moteur twig pour les vues
* [x] pattern Frontcontroleur avec routage
* [x] Javascript
* [x] pattern fabrique

## Fabriqué avec
* [JetBrains Toolbox](https://www.jetbrains.com/fr-fr/toolbox-app/) - IDE
* [CodeFirst](https://codefirst.iut.uca.fr/) - Gitea
  * [Drone](https://codefirst.iut.uca.fr/) - CI
  * [SonarQube](https://codefirst.iut.uca.fr/sonar/) - Qualité
* [PHP 8](https://www.php.net/downloads) - Langage
  * [html 5, CSS 3](https://developer.mozilla.org/fr/docs/Web/HTML) - Langage
  * [Twig](https://twig.symfony.com/) - Langage
* [Doxygen](https://codefirst.iut.uca.fr/sonar/) - Documentation


## Contributeurs
* [Antoine PEREDERII](https://codefirst.iut.uca.fr/git/antoine.perederii)
* [Paul LEVRAULT](https://codefirst.iut.uca.fr/git/paul.levrault)
* [Kevin MONTEIRO](https://codefirst.iut.uca.fr/git/kevin.monteiro)
* [Antoine PINAGOT](https://codefirst.iut.uca.fr/git/antoine.pinagot)
* [David D'HALMEIDA](https://codefirst.iut.uca.fr/git/david.d_almeida)

## Comment contribuer
1. Forkez le projet (<https://codefirst.iut.uca.fr/git/HeartDev/Web>)
2. Créez votre branche (`git checkout -b feature/featureName`)
3. commit vos changements (`git commit -am 'Add some feature'`)
4. Push sur la branche (`git push origin feature/featureName`)
5. Créez une nouvelle Pull Request


## License
Ce projet est sous licence ``MIT`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations.

## Remerciements
Ce projet a été réalisé dans le cadre de la SAÉ Projet Web et Mobile de l'IUT de Clermont-Ferrand.